﻿using System;

namespace UrlRewriterListGenerator
{
    static class Log
    {
        public static void Info(string message = "")
            => Console.WriteLine($"{message}{Environment.NewLine}");

        public static void Error(string message = "")
            => Console.WriteLine($"ERROR: {message}");
    }
}
