﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Linq;

namespace UrlRewriterListGenerator
{
    public static class ExcelExtensions
    {
        #region Worksheet methods

        public static ExcelWorksheet GetWorksheet(this ExcelPackage package, string name)
        {
            return package.Workbook.Worksheets[name];
        }

        public static ExcelWorksheet GetOrAddWorksheet(this ExcelPackage package, string name)
        {
            var worksheet = package.GetWorksheet(name);
            if (worksheet == null)
            {
                worksheet = package.AddWorksheet(name);
                worksheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                worksheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
            }
            return worksheet;
        }

        public static ExcelWorksheet AddWorksheet(this ExcelPackage package, string name)
        {
            return package.Workbook.Worksheets.Add(name);
        }

        #endregion

        #region Cell methods

        public static ExcelRangeBase FindByText(this ExcelRange cells, string text)
            => cells.FirstOrDefault(cell => cell.Text == text);

        public static int GetCol(this ExcelRangeBase cell)
            => cell?.Start?.Column ?? -1;

        public static void SetTitleCellText(this ExcelWorksheet worksheet, int row, int col, string title)
        {
            var cell = worksheet.Cells[row, col];
            cell.Value = title;
            cell.Style.Font.Size = 14;
            cell.Style.Font.Bold = true;
            cell.Style.Font.Italic = true;
        }

        public static void SetCellHeaderText(this ExcelWorksheet worksheet, int row, int col, string title)
        {
            var cell = worksheet.Cells[row, col];
            cell.Value = title;
            cell.Style.Font.Size = 12;
            cell.Style.Font.Bold = true;
        }

        public static void SetRangeBorders(this ExcelRangeBase range)
        {
            // border style
            range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Right.Style = ExcelBorderStyle.Thin;

            // border color
            range.Style.Border.Top.Color.SetColor(Color.LightGray);
            range.Style.Border.Bottom.Color.SetColor(Color.LightGray);
            range.Style.Border.Left.Color.SetColor(Color.LightGray);
            range.Style.Border.Right.Color.SetColor(Color.LightGray);
        }

        #endregion

        #region Column methods

        public static int InitColumn(this ExcelWorksheet worksheet, int row, int col,
            string headerText = null, double? width = null, bool wrapText = false)
        {
            var column = worksheet.Column(col);

            if (headerText != null) worksheet.SetCellHeaderText(row, col, headerText);

            if (width.HasValue) column.Width = width.Value;
            if (wrapText == true) column.Style.WrapText = wrapText;

            return col;
        }

        #endregion

        #region Row methods

        public static ExcelRange GetRowCells(this ExcelWorksheet worksheet, int rowIndex)
            => worksheet.Cells[rowIndex, worksheet.Dimension.Start.Column, rowIndex, worksheet.Dimension.End.Column];

        #endregion
    }
}
