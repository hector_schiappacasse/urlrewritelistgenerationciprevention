﻿using OfficeOpenXml;
using System;
using System.IO;

namespace UrlRewriterListGenerator
{
    public static class ExcelHelper
    {
        public static ExcelPackage Create(string path, bool force = false)
        {
            var fi = new FileInfo(path);
            if (fi.Exists)
                if (force)
                    fi.Delete();
                else
                    throw new ApplicationException($"Cannot create - file already exists: {fi.FullName}");

            return new ExcelPackage(fi);
        }

        public static ExcelPackage Open(string path)
        {
            var fi = new FileInfo(path);
            if (!fi.Exists)
                throw new FileNotFoundException($"File not found: {fi.FullName}");
            return new ExcelPackage(fi);
        }

        public static ExcelPackage CloneWithSuffix(string path, string suffix)
        {
            var clonePath = Path.Combine(
                Path.GetDirectoryName(path),
                string.Concat(
                    Path.GetFileNameWithoutExtension(path),
                    suffix,
                    Path.GetExtension(path)));

            File.Copy(path, clonePath);

            return Open(clonePath);
        }

        public static string PathWithSuffix(string path, string suffix)
        {
            return Path.Combine(
                Path.GetDirectoryName(path),
                string.Concat(
                    Path.GetFileNameWithoutExtension(path),
                    suffix,
                    Path.GetExtension(path)));
        }
    }
}
