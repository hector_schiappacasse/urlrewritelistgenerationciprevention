﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UrlRewriterListGenerator.Files;
using UrlRewriterListGenerator.Processors;
using static UrlRewriterListGenerator.Constants;

namespace UrlRewriterListGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                 switch (args.FirstOrDefault())
                {
                    case "RedirectCancerInformaionTest":
                        new UrlRewriterListGenerator.Processors.CancerInformation.RedirectCancerInformaionProcessor().Process();
                        break;

                    case "URLRedirectTest":
                        new RedirectTestProcessor().Process();
                        break;

                    case "MTTest":
                        new MigrationTrackerProcessor().Process();
                        break;

                    case "PLTest":
                        new PageListProcessor().Process();
                        break;

                    case "PreventionURLRedirect":

                        new Processors.PreventionCancerInformation.RedirectCancerPreventionProcessor().Process();
                        break;

                    default:
                        throw new ApplicationException("A command line argument is required.");
                }

                Log.Info();
                Log.Info($"Finished.");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
