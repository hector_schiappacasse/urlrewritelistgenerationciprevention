﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using static UrlRewriterListGenerator.Constants;

namespace UrlRewriterListGenerator
{
    public static class AppUtils
    {
        public static Uri EmptyUri = new Uri("http://__EmptyUri__");

        public static bool IsNullOrEmpty(Uri uri)
            => uri == null || uri == EmptyUri;

        public static Uri GetUri(string url)
        {
            if (Uri.TryCreate(url, UriKind.Absolute, out Uri uri))
                return uri;
            return EmptyUri;
        }


        public static string GetAbsolutePath(string url, string lang)
        {
            if (string.IsNullOrEmpty(url) || string.IsNullOrEmpty(lang))
            {
                return String.Empty;
            }
            else
            {
                var sb = new System.Text.StringBuilder("/");
                sb.Append(lang.Trim());
                sb.Append(url.Trim());
                sb.Replace(" ", "-");                
                return sb.ToString().ToLower();
            }
        }
        public static string GetAbsolutePath(string url)
        {
            var uri = AppUtils.GetUri(url);
            var subpath = uri.AbsolutePath;
            if (subpath.EndsWith("/")) subpath = subpath.Substring(0, subpath.Length - 1).ToLower();
            return subpath;
        }

        public static string GetItemStem(string url)
        {
            var uri = AppUtils.GetUri(url);
            var absolutePath = uri.AbsolutePath;

            var prefix = Constants.LanguagePrefixes.FirstOrDefault(p => absolutePath.StartsWith(p));
            var subpath = absolutePath.Substring(prefix?.Length ?? 0);
            if (subpath.StartsWith("/")) subpath = subpath.Substring(1);
            if (subpath.EndsWith("/")) subpath = subpath.Substring(0, subpath.Length - 1).ToLower();
            return subpath;
        }

        public static bool IsCancerCaSubpath(string subpath)
            => Constants.CancerCaPathPrefixes.Any(p => subpath.StartsWith(p));

        public static bool IsCancerCaMatchSubpath(string subpath)
            => Constants.CancerCaPathMatchPrefixes.Any(p => subpath.StartsWith(p));


        public static string GetRelativePath(string pathstr)
        {

            if (pathstr.IndexOf('/') == -1)
                return string.Empty;
            else
               return pathstr.Substring(pathstr.IndexOf('/')).ToLower();

        }

        public static string GetRelativePathFromAbsoluteUrl(string pathstr)
        {
            if (pathstr.IndexOf('/') == -1)
                return string.Empty;
            else
            {
                string pathAfterHttp = pathstr.Substring(pathstr.IndexOf('/') + 2);
                pathAfterHttp = pathAfterHttp.Replace("\n", "");
                if (pathAfterHttp.EndsWith("/")) pathAfterHttp = pathAfterHttp.Substring(0, pathAfterHttp.Length - 1).ToLower();
                return pathAfterHttp.Substring(pathAfterHttp.IndexOf('/'));
            }

            //return pathstr.Substring(pathstr.IndexOf('/'));

        }
        public static string RemovePrefixPathUrl(string pathstr)
        {
            return pathstr.Substring(pathstr.IndexOf("en/"));
           


            //return pathstr.Substring(pathstr.IndexOf('/'));

        }

        //public static Match GetPathMatch(Uri uri)
        //{
        //    var absolutePath = uri.AbsolutePath;
        //    return Regexs.AbsolutePathLanguage.Match(absolutePath);
        //}

        //public static 

        //public static Match GetPath(Regex )
        //{
        //    var absolutePath = uri.AbsolutePath;
        //    return Regexs.AbsolutePathLanguage.Match(absolutePath);
        //    if (match.Success)
        //    {
        //        var rest = match.Groups["rest"].Value;
        //        if (CancerCaPathPrefixes.Any(prefix => rest.StartsWith(prefix)))
        //        {
        //            redirect.RewriteMapName = "cancer";
        //            redirect.RedirectKey = match.Groups["rest"].Value.Substring(1);
        //        }

        //        else
        //        {
        //            redirect.RewriteMapName = "action";
        //            redirect.RedirectKey = match.Groups["rest"].Value.Substring(1);
        //        }
        //    }


        //}

        //public static string GetLanguageSegment(Uri uri)
        //{
        //    var absolutePath = uri.AbsolutePath;
        //    if (absolutePath.StartsWith("/fr-ca/"))
        //        return "fr-ca";
        //    if (absolutePath.StartsWith("/en/"))
        //        return "en";
        //    return null;
        //}

        //public static string SetHostName(string url, string hostName)
        //{
        //    var builder = new UriBuilder(url);
        //    builder.Host = hostName;
        //    var newUri = new Uri(builder.ToString());
        //    return newUri.GetComponents(UriComponents.AbsoluteUri & ~UriComponents.Port, UriFormat.UriEscaped);
        //}

        public static string FormatUrl(UriBuilder builder)
            => FormatUrl(new Uri(builder.ToString()));

        public static string FormatUrl(Uri uri)
            => uri.GetComponents(UriComponents.AbsoluteUri & ~UriComponents.Port, UriFormat.UriEscaped);
    }
}
