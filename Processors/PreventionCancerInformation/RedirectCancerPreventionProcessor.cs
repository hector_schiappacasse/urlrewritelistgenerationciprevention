﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrlRewriterListGenerator.Files.PreventionCancerInformation;
//using UrlRewriterListGenerator.Files.CancerInfomation;
using static UrlRewriterListGenerator.Constants;


namespace UrlRewriterListGenerator.Processors.PreventionCancerInformation
{
    internal class RedirectCancerPreventionProcessor
    {
        public void Process()
        {
            using (var pageListPackage = ExcelHelper.Open(PageListCancerPreventionInfoXls.Path))
            using (var cpreventionPackage = ExcelHelper.Open(CPreventionRedirectsXLs.Path))
            using (var cpreventionPagePackage = ExcelHelper.Open(Cancer9PagesInfoXls.Path))
            using (var redirectsPackage = ExcelHelper.Create(RedirectCancerPreventionInfoXLs.Path, true))
            {
                #region 1.reading XL data
                var cancerPageFile = new CancerPageList(cpreventionPagePackage);
                var camsUrls = cancerPageFile.GetAllUrls();

                var pageListFile = new PageListCancerInfoFile(pageListPackage);
                var pageSc7List = pageListFile.GetAllSourcePages();

                var cPreventioniRedirectFile = new PreventionRedirectsFile(cpreventionPackage);
                var ciRedirectList = cPreventioniRedirectFile.GetAllUrls();

                var cPreventionEarlyCureRedirectFile = new PreventionRedirectsFile(cpreventionPackage);
                var ciRedirectEarlyCureList = cPreventionEarlyCureRedirectFile.GetAllEarlyCureUrls();
                #endregion reading XL data

                #region 2.processing data
                var builder = new RedirectCIPreventionBuilder(camsUrls, pageSc7List, ciRedirectList);
                //var redirects = builder.GetRedirectsAll();
                var ciRedirectListUpdated = builder.ValidateCIVsCSFiles();

                var builderEarlyCure = new RedirectCIPreventionBuilder(camsUrls, pageSc7List, ciRedirectEarlyCureList);
                //var redirects = builder.GetRedirectsAll();
                var ciRedirectEarlyListUpdated = builderEarlyCure.ValidateCIVsCSFiles();


                #endregion processing data

                #region 3.saving result to XL spreadsheet
                var redirectsFile = new RedirectsCancerPreventionInfo(redirectsPackage);
                redirectsFile.AddRedirectsWorksheet(
                    RedirectCancerPreventionInfoXLs.WorksheetNames.Redirects,
                    ciRedirectListUpdated);
                redirectsFile.AddRedirectsWorksheet(
                    RedirectCancerPreventionEarlyCureInfoXLs.WorksheetNames.Redirects,
                    ciRedirectEarlyListUpdated);
                redirectsPackage.SaveAs(redirectsFile.Package.File);
                #endregion 3.saving result to XL spreadsheet
            }

        }

    }
}
