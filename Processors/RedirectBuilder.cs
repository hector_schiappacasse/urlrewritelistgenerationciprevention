﻿using System;
using System.Collections.Generic;
using System.Linq;
using UrlRewriterListGenerator.Files;
using static UrlRewriterListGenerator.Constants;

namespace UrlRewriterListGenerator.Processors
{
    public class RedirectBuilder
    {
        public List<Redirect> GetRedirectInfos(IEnumerable<CMTrackerRedirect> trackerRedirects)
        {
            // Done this way in case we have multiple sources of lists later.
            var combinedBaseRedirects = trackerRedirects.AsEnumerable<IRedirectBase>();
            ValidateRedirectInfos(combinedBaseRedirects);

            var redirects = combinedBaseRedirects
                .Select(tr => GetRedirectInfo(tr))
                .ToList();

            return redirects;
        }

        public void ValidateRedirectInfos(IEnumerable<IRedirectBase> redirects)
        {
            var duplicateSubpaths = (
                from redirect in redirects
                where redirect.Status.CanRedirect()
                let subpath = AppUtils.GetItemStem(redirect.FromUrl)
                group redirect by subpath into g
                where g.Count() > 1
                select g
                ).ToDictionary(
                g => g.Key,
                g => g.ToList());

            foreach (var pair in duplicateSubpaths)
                foreach (var redirect in pair.Value)
                    redirect.SetStatus(RedirectStatus.FromUrlDuplicate, pair.Key);
        }

        private Redirect GetRedirectInfo(IRedirectBase item)
        {
            var redirect = new Redirect
            {
                Source = item.Source,
                SourceRow = item.SourceRow,
                Status = item.Status,
                StatusText = item.StatusText,
                FromUrl = item.FromUrl,
                ToUrl = item.AdjustedToUrl ?? item.ToUrl
            };

            // Set rewrite information.
            redirect.RedirectKey = AppUtils.GetItemStem(redirect.FromUrl);
            switch (redirect.Status)
            {
                case RedirectStatus.OKActionRedirect:
                    redirect.RewriteMapName = MapNames.ActionCancerCaUrls;
                    redirect.RedirectValue = AppUtils.GetItemStem(redirect.ToUrl);
                    break;

                default:
                    redirect.RewriteMapName = MapNames.OtherHostUrls;
                    redirect.RedirectValue = redirect.ToUrl;
                    break;
            }

            return redirect;
        }
    }
}
