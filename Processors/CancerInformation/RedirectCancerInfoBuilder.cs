﻿using System;
using System.Collections.Generic;
using System.Linq;
using UrlRewriterListGenerator.Files;
using UrlRewriterListGenerator.Files.CancerInfomation;
using static UrlRewriterListGenerator.Constants;

namespace UrlRewriterListGenerator.Processors.CancerInformation
{
    public class RedirectCancerInfoBuilder
    {
        public IEnumerable<CamsPage> camsPages { get; set; }
        public IEnumerable<DlgKeysCancerInfo> dlgKeys { get; set; }
        public IEnumerable<CurrentPageName> currentPages { get; set; }
        public IEnumerable<PageSource> sc72Pages { get; set; }
        public IEnumerable<CIProposedRedirects> ciRedirects { get; set; }

        public RedirectCancerInfoBuilder(
            IEnumerable<CamsPage> _camsPages,
            IEnumerable<DlgKeysCancerInfo> _dlgKeys,
            IEnumerable<CurrentPageName> _currentPages,
            IEnumerable<PageSource> _sc72Pages,
            IEnumerable<CIProposedRedirects> _ciRedirects
        )
        {
            camsPages = _camsPages;
            dlgKeys = _dlgKeys;
            currentPages = _currentPages;
            sc72Pages = _sc72Pages;
            ciRedirects = _ciRedirects;

        }
        //public List<RedirectCancerInfo> GetRedirectInfos()
        //{
        //    List<RedirectCancerInfo> redirectMerged = GetRedirectsAll();
        //    //match CI data vs SC data:
        //    var matchResult = ValidateCIVsCSFiles();
        //    var SC72_NotIn_CI_From = matchResult.SC72_NotIn_CI_From;
        //    //return new List<RedirectCancerInfo>();
        //    //AddMissingPagesToCIRedirects(matchResult, ciRedirects);
        //    return redirectMerged;
        //}

        private void AddMissingPagesToCIRedirects()
        {

        }

        public List<RedirectCancerInfo> GetRedirectsAll()
        {
            var mergedList = camsPages
                .GroupJoin(
                    currentPages,
                    cams => new { cams.EzdId, cams.ItemLanguage },
                    curr => new { curr.EzdId, curr.ItemLanguage },
                    (cams, curr) => new { cams, curr }
                )
                .SelectMany(
                    x => x.curr.DefaultIfEmpty(),
                    (x, curr) => new
                    {
                        Sc93URLPath = x.cams.Sc93URLPath,
                        SourceRowSc93 = x.cams.SourceRow,
                        EzdId = curr.EzdId,
                        TocId = curr.TocId,
                        CceId = curr.CceId,
                        ItemLanguage = curr.ItemLanguage
                    }
                ).Distinct()
                .ToList();

            var mergedList2 = mergedList
                .GroupJoin(
                    dlgKeys,
                    r1 => new { r1.TocId, r1.CceId },
                    dk => new { dk.TocId, dk.CceId },
                    (r1, dk) => new { Left = r1, Rights = dk }
                )
                .SelectMany(
                    x => x.Rights.DefaultIfEmpty(),
                    (x, y) => new
                    {
                        ItemIdSC7 = y == null ? "" : y.ItemId,
                        Sc93URLPath = x.Left.Sc93URLPath,
                        SourceRowSc93 = x.Left.SourceRowSc93,
                        ItemLanguage = x.Left.ItemLanguage
                    }
                ).Distinct()
                .ToList();

            var redirectsLeftEn = mergedList2
                .GroupJoin(
                    sc72Pages,
                    r2 => r2.ItemIdSC7,
                    sc72 => sc72.ItemId,
                    (r2, sc72) => new { Left = r2, Rights = sc72 }
                )
                .SelectMany(
                    x => x.Rights.DefaultIfEmpty(),
                    (x, ps) => new
                    {
                        SourceRowSc93 = x.Left.SourceRowSc93,
                        SourceRowSc72 = ps == null ? 0 : ps.SourceRow,
                        FromUrl = ps == null ? "" : ps.EnglishUrl,
                        ToUrl = x.Left.Sc93URLPath,
                        ItemId = ps == null ? "" : ps.ItemId,
                        ItemLanguage = x.Left.ItemLanguage
                    }
                )
                .Where(x => x.ItemLanguage == "en")
                .Distinct()
                .ToList();

            var redirectsRightEn = sc72Pages
                .GroupJoin(
                    mergedList2,
                    sc72 => sc72.ItemId,
                    r2 => r2.ItemIdSC7,
                    (sc72, r2) => new { Left = sc72, Rights = r2 }
                )
                .SelectMany(
                    x => x.Rights.DefaultIfEmpty(),
                    (ps, x2) => new
                    {
                        SourceRowSc93 = x2 == null ? 0 : x2.SourceRowSc93,
                        SourceRowSc72 = ps.Left.SourceRow,
                        FromUrl = ps.Left.EnglishUrl,
                        ToUrl = x2 == null ? "" : x2.Sc93URLPath,
                        ItemId = x2 == null ? ps.Left.ItemId : x2.ItemIdSC7,
                        ItemLanguage = x2 == null ? "" : x2.ItemLanguage
                    }
                )
                .Where(x => x.ItemLanguage == "en")
                .Distinct()
                .ToList();

            var redirectsLeftFr = mergedList2
                .GroupJoin(
                    sc72Pages,
                    r2 => r2.ItemIdSC7,
                    sc72 => sc72.ItemId,
                    (r2, sc72) => new { Left = r2, Rights = sc72 }
                )
                .SelectMany(
                    x => x.Rights.DefaultIfEmpty(),
                    (x, ps) => new
                    {
                        SourceRowSc93 = x.Left.SourceRowSc93,
                        SourceRowSc72 = ps == null ? 0 : ps.SourceRow,
                        FromUrl = ps == null ? "" : ps.FrenchUrl,
                        ToUrl = x.Left.Sc93URLPath,
                        ItemId = ps == null ? "" : ps.ItemId,
                        ItemLanguage = x.Left.ItemLanguage
                    }
                )
                .Where(x => x.ItemLanguage == "fr")
                .Distinct()
                .ToList();

            var redirectsRightFr = sc72Pages
                .GroupJoin(
                    mergedList2,
                    sc72 => sc72.ItemId,
                    r2 => r2.ItemIdSC7,
                    (sc72, r2) => new { Left = sc72, Rights = r2 }
                )
                .SelectMany(
                    x => x.Rights.DefaultIfEmpty(),
                    (ps, x2) => new
                    {
                        SourceRowSc93 = x2 == null ? 0 : x2.SourceRowSc93,
                        SourceRowSc72 = ps.Left.SourceRow,
                        FromUrl = ps.Left.FrenchUrl,
                        ToUrl = x2 == null ? "" : x2.Sc93URLPath,
                        ItemId = x2 == null ? ps.Left.ItemId : x2.ItemIdSC7,
                        ItemLanguage = x2 == null ? "" : x2.ItemLanguage
                    }
                )
                .Where(x => x.ItemLanguage == "fr")
                .Distinct()
                .ToList();

            var redirects = redirectsLeftEn.Union(redirectsRightEn).Union(redirectsLeftFr).Union(redirectsRightFr).Distinct()
                .Select(x => new RedirectCancerInfo
                {
                    Language = x.ItemLanguage,
                    ToUrl = x.ToUrl,
                    FromUrl = x.FromUrl,
                    SourceRowSc93 = x.SourceRowSc93,
                    SourceRowSc72 = x.SourceRowSc72
                }
                ).Distinct().ToList();

            var redirectsRight = redirects
                .GroupJoin(
                    ciRedirects,
                    r1 => r1.ToUrl,
                    cir => cir.ToUrl,
                    (r1, cir) => new { Left = r1, Rights = cir }
                )
                .SelectMany(
                    x => x.Rights.DefaultIfEmpty(),
                    (x1, x2) =>
                    {
                        var rdrct = x1.Left;
                        var cir = x2;
                        return new RedirectCancerInfo
                        {
                            Language = rdrct.Language,
                            SourceCIRow = cir == null ? 0 : cir.SourceCIRow,
                            SourceRowSc72 = rdrct.SourceRowSc72,
                            SourceRowSc93 = rdrct.SourceRowSc93,
                            FromUrl = rdrct.FromUrl,
                            ToUrl = rdrct.ToUrl,
                            FromUrlCI = cir == null ? String.Empty : cir.FromUrl,
                            ToUrlCI = cir == null ? String.Empty : cir.ToUrl,
                        };
                    }
                ).Distinct()
                .ToList();

            var redirectsLeft = ciRedirects
                .GroupJoin(
                    redirects,
                    cir => cir.ToUrl,
                    r1 => r1.ToUrl,
                    (cir, r1) => new { Left = cir, Rights = r1 }
                )
                .SelectMany(
                    x => x.Rights.DefaultIfEmpty(),
                    (x1, x2) =>
                    {
                        var cir = x1.Left;
                        var rdrct = x2;
                        return new RedirectCancerInfo
                        {
                            Language = rdrct == null ? String.Empty : rdrct.Language,
                            SourceCIRow = cir.SourceCIRow,
                            SourceRowSc72 = rdrct == null ? 0 : rdrct.SourceRowSc72,
                            SourceRowSc93 = rdrct == null ? 0 : rdrct.SourceRowSc93,
                            FromUrl = rdrct == null ? String.Empty : rdrct.FromUrl,
                            ToUrl = rdrct == null ? String.Empty : rdrct.ToUrl,
                            FromUrlCI = cir.FromUrl,
                            ToUrlCI = cir.ToUrl,
                        };
                    }
                ).Distinct()
                .ToList();

            var redirectMerged = redirectsRight.Union(redirectsLeft).Distinct().ToList();
            return redirectMerged;
        }

        internal object GetCiRedirectListUpdated()
        {
            throw new NotImplementedException();
        }

        internal IEnumerable<CIProposedRedirects> ValidateCIVsCSFiles()
        {
            var camsSc93URLPath = camsPages.Where(y => y.ItemLanguage == "en").Select(y => y.Sc93URLPath).ToList();
            var CI_NotIn_Cams_To = ciRedirects
                .Where(x =>
                {
                    bool Is_ci_NotIn_Cams_To = camsSc93URLPath.All(y => y != x.ToUrl);
                    if (Is_ci_NotIn_Cams_To) x.StatusSC9 = CIredirectStatus.CI_NotIn_SC9;
                    return Is_ci_NotIn_Cams_To;
                })
                .ToList();

            var sc72URLs = sc72Pages.Select(y => y.EnglishUrl).ToList();
            var CI_NotIn_SC72_From = ciRedirects
                .Where(x =>
                {
                    bool Is_ci_NotIn_SC72_To = sc72URLs.All(y => y != x.FromUrl);
                    if (Is_ci_NotIn_SC72_To) x.StatusSC7 = CIredirectStatus.CI_NotIn_SC7;
                    return Is_ci_NotIn_SC72_To;
                })
                .ToList();


            var SC72_NotIn_CI_From = sc72Pages
                .Select(x => x.EnglishUrl)
                .Except(ciRedirects.Select(x => x.FromUrl))
                .Select(x =>
                {
                    return new CIProposedRedirects
                    {
                        FromUrl = x,
                        StatusSC7 = CIredirectStatus.SC72_NotIn_CI,
                        StatusSC9 = CIredirectStatus.NotApplicable
                    };
                })
                .ToList();

            ciRedirects = ciRedirects.Concat(SC72_NotIn_CI_From).ToList();

            #region comments
            //var ciRedirectsFromURLs = ciRedirects.Select(y => y.FromUrl);
            //var SC72_NotIn_CI_From = sc72Pages
            //    .Where(x => ciRedirectsFromURLs.All(y => y != x.EnglishUrl))
            //    .ToList();
            //var CI_NotIn_Cams_To_2 = ciRedirects
            //    .Select(x =>
            //    {
            //        string path = x.ToUrl;
            //        if (path.EndsWith("/")) path = path.Substring(0, path.Length - 1);
            //        return path;
            //    })
            //    .Except(camsPages
            //        .Where(x => x.ItemLanguage == "en")
            //        .Select(x =>
            //        {
            //            string path = AppUtils.GetAbsolutePath(x.ContentPath, "en");
            //            return path;
            //        }))
            //    .ToList();
            //var ciRedirectsToURLs = ciRedirects.Select(y => y.ToUrl);
            //var Cams_NotIn_CI_To = camsPages
            //    .Where(x =>
            //    {
            //        bool Is_Cams_NotIn_CI_To = x.ItemLanguage == "en" && ciRedirectsToURLs.All(y => y != x.Sc93URLPath);
            //        //if (Is_Cams_NotIn_CI_To) x.Status = CIredirectStatus.Cams_NotIn_CI;
            //        return Is_Cams_NotIn_CI_To;
            //    })
            //    .ToList();
            //var Cams_NotIn_CI_To_2 = camsPages
            //    .Where(x => x.ItemLanguage == "en")
            //    .Select(x =>
            //    {
            //        var path = AppUtils.GetAbsolutePath(x.ContentPath, "en");
            //        return new
            //        {
            //            fromUrl = path
            //        };
            //    })
            //    .Except(ciRedirects.Select(x =>
            //    {
            //        var path = x.ToUrl;
            //        if (path.EndsWith("/")) path = path.Substring(0, path.Length - 1);
            //        return new
            //        {
            //            fromUrl = path
            //        };
            //    }))
            //    .ToList();
            //var CI_NotIn_SC72_From_2 = ciRedirects
            //    .Select(x =>
            //    {
            //        var path = "/en/" + AppUtils.GetItemStem(x.FromUrl);
            //        return new
            //        {
            //            fromUrl = path
            //        };
            //    })
            //    .Except(sc72Pages.Select(x =>
            //    {
            //        var path = x.EnglishUrl;
            //        if (path.EndsWith("/")) path = path.Substring(0, path.Length - 1);
            //        return new
            //        {
            //            fromUrl = path
            //        };
            //    }))
            //    .ToList();
            #endregion comments

            return ciRedirects;
        }

    }


}
