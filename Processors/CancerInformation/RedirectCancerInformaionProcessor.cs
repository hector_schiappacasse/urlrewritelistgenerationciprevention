﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using UrlRewriterListGenerator.Files;
using UrlRewriterListGenerator.Files.CancerInfomation;
using static UrlRewriterListGenerator.Constants;


namespace UrlRewriterListGenerator.Processors.CancerInformation
{
    internal class RedirectCancerInformaionProcessor
    {
        public void Process()
        {
            using (var pageListPackage = ExcelHelper.Open(PageListCancerInfoXls.Path))
            using (var ciRedirectsPackage = ExcelHelper.Open(CIProposedRedirectsXLs.Path))
            using (var camsPackage = ExcelHelper.Open(CamsPageXLs.Path))
            using (var currentPagePackage = ExcelHelper.Open(CurrentPageNamesXLs.Path))
            using (var dlgKeysPackage = ExcelHelper.Open(DlgKeysCancerInfoXLs.Path))
            using (var redirectsPackage = ExcelHelper.Create(RedirectCancerInfoXLs.Path, true))
            {
                #region 1.reading XL data
                var camsFile = new CamsPageFile(camsPackage);
                var camsUrls = camsFile.GetAllUrls();

                var currentPageFile = new CurrentPageNameFile(currentPagePackage);
                var currentPageUrls = currentPageFile.GetAllKeys();

                var dlgKeysFile = new DlgKeysCancerInfoFile(dlgKeysPackage);
                var dlgKeys = dlgKeysFile.GetAllKeys();

                var pageListFile = new PageListCancerInfoFile(pageListPackage);
                var pageSc7List = pageListFile.GetAllSourcePages();

                var ciRedirectFile = new CIProposedRedirectsFile(ciRedirectsPackage);
                var ciRedirectList = ciRedirectFile.GetAllUrls();
                #endregion reading XL data

                #region 2.processing data
                var builder = new RedirectCancerInfoBuilder(camsUrls, dlgKeys, currentPageUrls, pageSc7List, ciRedirectList);
                //var redirects = builder.GetRedirectsAll();
                var ciRedirectListUpdated = builder.ValidateCIVsCSFiles();
                #endregion processing data

                #region 3.saving result to XL spreadsheet
                var redirectsFile = new RedirectsCancerInfo(redirectsPackage);
                //redirectsFile.AddRedirectsWorksheet(
                //    RedirectCancerInfoXLs.WorksheetNames.Redirects,
                //    redirects);
                redirectsFile.AddRedirectsWorksheet(
                    CIProposedRedirectsXLs.WorksheetNames.CIProposedRedirectsProcessed,
                    ciRedirectListUpdated);
                redirectsPackage.SaveAs(redirectsFile.Package.File);
                #endregion 3.saving result to XL spreadsheet
            }

        }

    }
}