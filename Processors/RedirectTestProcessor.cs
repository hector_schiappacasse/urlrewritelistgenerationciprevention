﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using UrlRewriterListGenerator.Files;
using static UrlRewriterListGenerator.Constants;


namespace UrlRewriterListGenerator.Processors
{
    internal class RedirectTestProcessor
    {
        private HttpClient _httpClient;
        private HttpClient Client
        {
            get
            {
                if (_httpClient == null)
                {
                    var httpClientHandler = new HttpClientHandler()
                    {
                        AllowAutoRedirect = false,
                    };
                    _httpClient = new HttpClient(httpClientHandler);
                    _httpClient.DefaultRequestHeaders.ConnectionClose = true;
                }

                return _httpClient;
            }
        }

        public RedirectTestProcessor()
        {
        }

        private string redirectUrl;
        Uri redirectUri = null;

        internal async void Process()
        {
            var redirectTestURLs = new List<RedirectTest>();
            var responses = new List<RedirectTest>();

            using (var redirectsPackage = ExcelHelper.Open(RedirectTestXLs.Path))
            {
                var redirectTestFile = new RedirectTestFile(redirectsPackage);
                redirectTestURLs = redirectTestFile.GetAllRedirects();

                using (var client = Client)
                {
                    redirectTestURLs.ForEach(x =>
                    {
                        RedirectTesMatchStatus matchStatus = RedirectTesMatchStatus.NotApplicable;
                        string xOldUrl = x.OldUrl.Trim();
                        string sc72Url = xOldUrl.EndsWith("/") ? xOldUrl.Substring(0, xOldUrl.Length - 1) : xOldUrl;
                        string xNewUrl = x.NewUrl.Trim();
                        string sc93Url = xNewUrl.EndsWith("/") ? xNewUrl.Substring(0, xNewUrl.Length - 1) : xNewUrl;
                        Uri resultUri = null;
                        Uri sc72Uri = null;
                        Uri sc93Uri = null;
                        bool sc72Valid = Uri.IsWellFormedUriString(sc72Url, UriKind.Absolute) && Uri.TryCreate(sc72Url, UriKind.Absolute, out sc72Uri);
                        bool sc93Valid = Uri.IsWellFormedUriString(sc93Url, UriKind.Absolute) && Uri.TryCreate(sc93Url, UriKind.Absolute, out sc93Uri);
                        if (sc93Valid && sc72Valid)
                        {
                            HttpResponseMessage response = GetRedirectUrl(sc72Url, client);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                resultUri = response.RequestMessage.RequestUri;
                                matchStatus = (sc93Uri == resultUri) ? RedirectTesMatchStatus.Match : RedirectTesMatchStatus.NotMatch;
                            }
                            else
                            {
                                Console.WriteLine("URL is not valid: " + sc72Uri);
                                matchStatus = RedirectTesMatchStatus.NotApplicable;
                            }
                        }
                        else
                        {
                            matchStatus = RedirectTesMatchStatus.NotApplicable;
                        }

                        Console.WriteLine("-------------- " + x.SourceRow + " => " + matchStatus + " --------------");

                        var redirectTest = new RedirectTest()
                        {
                            SourceRow = x.SourceRow,
                            NewUrl = sc93Url,
                            OldUrl = sc72Url,
                            RedirectUrl = resultUri?.ToString()
                        };
                        redirectTest.SetStatus(matchStatus, matchStatus.ToString());

                        responses.Add(redirectTest);
                    });
                }
            }
            // have to start over to make Save working:
            using (var redirectsPackage = ExcelHelper.Open(RedirectTestXLs.Path))
            {
                var redirectTestFile = new RedirectTestFile(redirectsPackage);
                redirectTestFile.UpdateStatus(responses);
            }

        }

        private HttpResponseMessage GetRedirectUrl(string url, HttpClient client)
        {
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(url),
                Method = HttpMethod.Get
            };

            HttpResponseMessage response = client.SendAsync(request).Result;
            var statusCode = (int)response.StatusCode;

            // We want to handle redirects ourselves so that we can determine the final redirect Location (via header)
            if (statusCode >= 300 && statusCode <= 399)
            {
                redirectUri = response.Headers.Location;
                if (!redirectUri.IsAbsoluteUri)
                {
                    redirectUri = new Uri(request.RequestUri.GetLeftPart(UriPartial.Authority) + redirectUri);
                }
                //_status.AddStatus(string.Format("Redirecting to {0}", redirectUri));
                return GetRedirectUrl(redirectUri.ToString(), client);
            }
            else if (!response.IsSuccessStatusCode)
            {
                throw new Exception();
            }
            else if (statusCode == 200)
            {
                return response;
            }

            return response;
        }

        /*
                public string MakeRequest(string url)
                {
                    HttpClientHandler httpClientHandler = new HttpClientHandler();
                    httpClientHandler.AllowAutoRedirect = false;

                    using (var client = new HttpClient(httpClientHandler))
                    {
                        var request = new HttpRequestMessage()
                        {
                            RequestUri = new Uri(url),
                            Method = HttpMethod.Get
                        };

                        HttpResponseMessage response = client.SendAsync(request).Result;
                        var statusCode = (int)response.StatusCode;

                        // We want to handle redirects ourselves so that we can determine the final redirect Location (via header)
                        if (statusCode >= 300 && statusCode <= 399)
                        {
                            redirectUri = response.Headers.Location;
                            if (!redirectUri.IsAbsoluteUri)
                            {
                                redirectUri = new Uri(request.RequestUri.GetLeftPart(UriPartial.Authority) + redirectUri);
                            }
                            //_status.AddStatus(string.Format("Redirecting to {0}", redirectUri));
                            return MakeRequest(redirectUri.ToString());
                        }
                        else if (!response.IsSuccessStatusCode)
                        {
                            throw new Exception();
                        }

                        return url;
                    }
                }

                async Task zzz()
                {
                    HttpClientHandler httpClientHandler = new HttpClientHandler();
                    httpClientHandler.AllowAutoRedirect = true;
                    using (HttpClient client = new HttpClient(httpClientHandler))
                    {

                        //string response = await client.GetStringAsync("https://cancer.ca/en/research");
                        HttpResponseMessage response = await client.GetAsync("http://cancer.ca/en/research");
                        //var res = response.Content;
                    }
                    return;
                }

                async Task<List<RedirectTest>> Xxx22(List<RedirectTest> redirectTestURLs)
                {
                    string result;
                    var responses = new List<RedirectTest>();

                    HttpClientHandler httpClientHandler = new HttpClientHandler();
                    httpClientHandler.AllowAutoRedirect = false;
                    using (HttpClient client = new HttpClient(httpClientHandler))
                    {
                        redirectTestURLs.ForEach(async (x) =>
                        {
                            string sc72Url = x.OldUrl.Trim();// "http://www.cancer.ca/en/research/";
                            Uri resultUri = null; ;
                            Uri sc72Uri = null;
                            bool sc72Valid = Uri.IsWellFormedUriString(sc72Url, UriKind.Absolute) && Uri.TryCreate(sc72Url, UriKind.Absolute, out sc72Uri);
                            string sc93Url = x.NewUrl.Trim();// "http://www.cancer.ca/en/research/";
                            Uri sc93Uri = null;
                            bool sc93Valid = Uri.IsWellFormedUriString(sc93Url, UriKind.Absolute) && Uri.TryCreate(sc93Url, UriKind.Absolute, out sc93Uri);
                            if (sc93Valid && sc72Valid)
                            {
                                HttpResponseMessage response = await client.GetAsync(sc72Uri).ConfigureAwait(false);
                                //using ()
                                //{
                                if (response.StatusCode == System.Net.HttpStatusCode.MovedPermanently)
                                {
                                    resultUri = response.Headers.Location;
                                    var redirectTest = new RedirectTest()
                                    {
                                        NewUrl = sc93Uri.ToString(),
                                        OldUrl = sc72Url.ToString(),
                                        RedirectUrl = resultUri.ToString()
                                    };
                                    responses.Add(redirectTest);

                                    Console.WriteLine("True " + resultUri);
                                    if (sc93Uri == resultUri)
                                    {
                                        Console.WriteLine("Redirect Match " + resultUri);
                                    }
                                    else
                                    {
                                        Console.WriteLine("Redirect Not Match " + resultUri);
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("False " + sc72Uri);
                                }

                                Console.WriteLine(response.Headers.ToString());

                                if (response.IsSuccessStatusCode)
                                {
                                    result = ((int)response.StatusCode).ToString();
                                }
                                else
                                {
                                    result = ((int)response.StatusCode).ToString();
                                }
                                Console.WriteLine(result);
                                //}
                            }
                        });
                    }

                    return responses;


                }
        */

    }
}