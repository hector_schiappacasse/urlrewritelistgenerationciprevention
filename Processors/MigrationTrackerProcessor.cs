﻿using System;
using System.Linq;
using UrlRewriterListGenerator.Files;
using static UrlRewriterListGenerator.Constants;

namespace UrlRewriterListGenerator.Processors
{
    /// <summary>
    /// A processor created to test processing of the migration tracker.
    /// </summary>
    public class MigrationTrackerProcessor
    {
        public void Process()
        {
            //var suffix = DateTime.Now.ToString("-yyMMdd-HHmmss");
            //var redirectsPath = ExcelHelper.PathWithSuffix(RedirectsXls.Path, suffix);

            using (var trackerPackage = ExcelHelper.Open(ContentMigrationTrackerXls.Path))
            using (var redirectsPackage = ExcelHelper.Create(RedirectsXls.Path, true))
            {
                var trackerFile = new CMTrackerFile(trackerPackage);

                var trackerRedirects = trackerFile.GetAllRedirects();

                var redirectsFile = new RedirectsFile(redirectsPackage);

                var builder = new RedirectBuilder();
                var redirects = builder.GetRedirectInfos(
                    trackerRedirects.Where(r => r.Status.CanRedirect()));

                redirectsFile.AddRedirectsWorksheet(
                    RedirectsXls.WorksheetNames.RedirectsMap,
                    redirects);

                redirectsFile.AddRedirectsWorksheet(
                    RedirectsXls.WorksheetNames.AllRedirects,
                    trackerRedirects.OrderBy(r => r.SourceRow));

                redirectsPackage.SaveAs(redirectsFile.Package.File);

                UrlRewriterMapFile.CreateFile(UrlRewriterMapConfig.Path, redirects);
            }
        }
    }
}
