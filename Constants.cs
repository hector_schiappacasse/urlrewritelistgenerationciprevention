﻿using System.Text.RegularExpressions;

namespace UrlRewriterListGenerator
{
    public static class Constants
    {
        private static readonly string dataDir;

        static Constants()
        {
            dataDir = @"C:\projects\UrlRewriterListGenerator\App_Data\_testdata";
        }

        #region Excel files

        public static class ContentMigrationTrackerXls
        {
            public static string Path { get; } = $@"{dataDir}\Cancer.ca content migration tracker.xlsx";

            public static class WorksheetNames
            {
                public static string Redirects { get; } = "Cancer.ca content migration tra";
            }

            public static class ColumnNames
            {
                public static string Included = "Include in Mar 31 launch";
                public static string FromUrl = "Old cancer.ca page URL (Eng only)";
                public static string ToUrl = "New cancer.ca page URL (Eng only)";
            }
        }

        public static class RedirectsXls
        {
            public static string Path { get; } = $@"{dataDir}\Redirects Workbook.xlsx";

            public static class WorksheetNames
            {
                public static string AllRedirects { get; } = "All redirects";
                public static string RedirectsMap { get; } = "Redirects map";
                //public static string Redirects { get; } = "All Redirects";
                //public static string IncludedInvalidRedirects { get; } = "Included invalid redirects";
                //public static string IncludedValidRedirects { get; } = "Included valid redirects";
                public static string PageListSource { get; } = "Page List Source";
            }

            //public static class ColumnNames
            //{
            //    public static string Source = "Source";
            //    public static string SourceRowIndex = "Source Row";
            //    public static string Included = "Included";
            //    public static string FromUrl = "From Url";
            //    public static string FromHost = "From Host";
            //    public static string ToUrl = "To Url";
            //    public static string ToHost = "To Host";
            //}
        }

        public static class PageListXls
        {
            public static string Path { get; } = $@"{dataDir}\PageList_20210104_0857_Pages.xlsx";

            public static class WorksheetNames
            {
                public static string PageList { get; } = "Page List for sc.cancer.ca";
            }

            public static class ColumnNames
            {
                public static string ItemId = "ID";
                public static string ItemPath = "Path";
                public static string EnglishTitle = "English Title";
                public static string EnglishUrl = "English Url";
                public static string FrenchTitle = "French Title";
                public static string FrenchUrl = "French Url";
            }
        }

        public static class PageListCancerInfoXls
        {
            //public static string Path { get; } = $@"{dataDir}\PageList_20210104_0857_Pages.xlsx";
            public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\PageList_20210720_1542_Pages-Cancer Info only.xlsx";

            public static class WorksheetNames
            {
                public static string PageList { get; } = "Page List for sc.cancer.ca";
            }

            public static class ColumnNames
            {
                public static string ItemId = "ID";
                public static string ItemPath = "Path";
                public static string EnglishTitle = "English Title";
                public static string EnglishUrl = "English Url";
                public static string FrenchTitle = "French Title";
                public static string FrenchUrl = "French Url";
            }
        }

        public static class RedirectSources
        {
            public static string ContentMigrationTracker = "CMTrk";
            public static string PageListSource = "SC72Pages";
        }

        public static class RedirectTestXLs
        {
            public static string Path { get; } = $@"{dataDir}\Cancer.ca For Researchers URLs Bkp.xlsx";

            public static class WorksheetNames
            {
                public static string Redirects { get; } = "Research";
            }

            public static class ColumnNames
            {
                public static string SC72 = "Old PROD cancer.ca page URL (Eng only)";
                public static string SC93 = "New PROD active.cancer.ca page URL (Eng only)";
                public static string RedirectStatus = "Redirect Status";
            }
        }

        public static class RedirectCancerInfoXLs
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\RedirectCancerInfo.xlsx";

            public static class WorksheetNames
            {
                public static string Redirects { get; } = "Redirects";
            }

            public static class ColumnNames
            {
                public static string RedirectStatus = "Redirect Status";
                public static string RedirectStatusText = "Redirect Status Text";
                public static string Language = "Language";
                public static string SC72 = "Old PROD cancer.ca page URL";
                public static string SC93 = "New PROD active.cancer.ca page URL";
            }
        }
        
        public static class CIProposedRedirectsXLs
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\Cancerca_CI-URL_consolidated.xlsx";

            public static class WorksheetNames
            {
                public static string CIProposedRedirects { get; } = "sheet1";
                public static string CIProposedRedirectsProcessed { get; } = "CI Redirects Processed";
            }

            public static class ColumnNames
            {
                public static string FromUrl = "English Url";
                public static string ToUrl = "Proposed URL";
            }
        }
        
        public static class CamsPageXLs
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\CAMS 9 Pages-21-07-26a.xlsx";

            public static class WorksheetNames
            {
                public static string CamsPages { get; } = "CAMS 9 Pages-21-07-26a";
            }

            public static class ColumnNames
            {
                public static string ContentPath = "ContentPath";
                public static string ContentItemID = "ContentItemID";
                public static string ItemLanguage = "Language";
                public static string EzdId = "EzdId";
            }
        }

        public static class DlgKeysCancerInfoXLs
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\DlgKeysCancerInfo.xlsx";

            public static class WorksheetNames
            {
                public static string DlgKeysCancerInfo { get; } = "DlgKeysCancerInfo";
            }

            public static class ColumnNames
            {
                public static string ContentPath = "ContentPath";
                public static string ItemId = "ItemId";
                public static string EzdId = "EzdId";
                public static string TocId = "TocId";
                public static string CceId = "CceId";
            }
        }

        public static class CurrentPageNamesXLs
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\Current Page names-210722a.xlsx";

            public static class WorksheetNames
            {
                public static string CurrentPageNames { get; } = "sheet1";
            }

            public static class ColumnNames
            {
                public static string EzdIdEn = "EnEzdIdText";
                public static string EzdIdFr = "FrEzdIdText";
                public static string CceTocIdEn = "EnName";
                public static string CceTocIdFr = "FrName";
            }
        }

        public static class CPreventionRedirectsXLs
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoPrevention\Prev_URL_redirects_Aug2021.xlsx";

            public static class WorksheetNames
            {
                public static string CPreventionRedirects { get; } = "Reduce your risk";
                public static string CPreventionRedirectsProcessed { get; } = "Find cancer early";
            }

            public static class ColumnNames
            {
                public static string OldPageTitleEn = "Old page title - EN";
                public static string OldPageTitleFr = "Old page title - FR";
                public static string FromUrl = "Old URL - EN";
                public static string newPageTitleEn = "New page title - EN";
                public static string ToUrl = "New URL - EN";
                public static string Notes = "Notes";
      
            }
        }

        public static class PageListCancerPreventionInfoXls
        {
            //public static string Path { get; } = $@"{dataDir}\PageList_20210104_0857_Pages.xlsx";
            public static string Path { get; } = $@"{dataDir}\CancerInfoPrevention\PageList_20210720_1542_Pages.xlsx";

            public static class WorksheetNames
            {
                public static string PageList { get; } = "Page List for sc.cancer.ca";
            }

            public static class ColumnNames
            {
                public static string ItemId = "ID";
                public static string ItemPath = "Path";
                public static string EnglishTitle = "English Title";
                public static string EnglishUrl = "English Url";
                public static string FrenchTitle = "French Title";
                public static string FrenchUrl = "French Url";
            }
        }

        public static class Cancer9PagesInfoXls
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoPrevention\Cancer 9 Pages List.xlsx";

            public static class WorksheetNames
            {
                public static string CanPrevPages { get; } = "Cancer 9 Pages List";
            }

            public static class ColumnNames
            {
             
                public static string ContentItemID = "ID";
                public static string ContentSC93 = "Path";


            }
        }

        public static class RedirectCancerPreventionInfoXLs
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\RedirectCancerPreventionInfo.xlsx";

            public static class WorksheetNames
            {
                public static string Redirects { get; } = "Reduce your Risk";
            }

            public static class ColumnNames
            {
                public static string OldPageTitleEn = "Old page title - EN";
                public static string OldPageTitleFr = "Old page title - FR";
                public static string FromUrl = "Old URL - EN";
                public static string newPageTitleEn = "New page title - EN";
                public static string ToUrl = "New URL - EN";
                public static string Notes = "Notes";
            }
        }

        public static class RedirectCancerPreventionEarlyCureInfoXLs
        {
            public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\RedirectCancerPreventionInfo.xlsx";

            public static class WorksheetNames
            {
                public static string Redirects { get; } = "Find Cancer Early";
            }

            public static class ColumnNames
            {
                public static string OldPageTitleEn = "Old page title - EN";
                public static string OldPageTitleFr = "Old page title - FR";
                public static string FromUrl = "Old URL - EN";
                public static string newPageTitleEn = "New page title - EN";
                public static string ToUrl = "New URL - EN";
                public static string Notes = "Notes";
            }
        }

        //public static class PageListCancerInfoXls
        //{
        //    public static string Path { get; } = $@"{dataDir}\CancerInfoRedirects\PageList_20210720_1542_Pages-Cancer Info only.xlsx";

        //    public static class WorksheetNames
        //    {
        //        public static string PageList { get; } = "Page List for sc.cancer.ca";
        //    }

        //    public static class ColumnNames
        //    {
        //        public static string ItemId = "ID";
        //        public static string ItemPath = "Path";
        //        public static string EnglishUrl = "English Url";
        //        public static string FrenchUrl = "French Url";
        //    }
        //}


        #endregion


        public readonly static string[] LanguagePrefixes = new string[]
        {
            "/en",
            "/fr-ca",
            "/fr"
        };

        public static string[] CancerCaPathPrefixes =
        {
            "cancer-information",
            "prevention-and-screening",
            "research"
        };

        public static string[] CancerCaPathMatchPrefixes =
        {
            "cancer-information",
            "prevention-and-screening"
        };


        public static class MapNames
        {
            public static readonly string ActionCancerCaUrls = "action";
            public static readonly string OtherHostUrls = "other";
        }

        #region UrlRewriter Rewrite map file

        public static class UrlRewriterMapConfig
        {
            public static string Path { get; } = $@"{dataDir}\RewriteMaps.config";
        }

        #endregion
    }
}
