﻿using System;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files
{
    public class CMTrackerRedirect : IRedirectBase
    {
        public string Source { get; set; }

        [Description("Source Row")]
        public int SourceRow { get; set; }

        [Description("Status")]
        public RedirectStatus Status { get; set; }

        [Description("Status Text")]
        public string StatusText { get; set; }

        [Description("Included")]
        public bool IsIncluded { get; set; }

        [Description("From Url")]
        public string FromUrl { get; set; }

        [Description("To Url")]
        public string ToUrl { get; set; }

        [Description("Adjusted To Url")]
        public string AdjustedToUrl { get; set; }

        public void ComputeStatus()
        {
            if (!IsIncluded)
            {
                SetStatus(RedirectStatus.Excluded);
                return;
            }

            if (string.IsNullOrWhiteSpace(FromUrl))
            {
                SetStatus(RedirectStatus.FromUrlMissing);
                return;
            }

            if (string.IsNullOrWhiteSpace(ToUrl))
            {
                SetStatus(RedirectStatus.ToUrlMissing);
                return;
            }

            var fromUri = AppUtils.GetUri(FromUrl);
            if (AppUtils.IsNullOrEmpty(fromUri))
            {
                SetStatus(RedirectStatus.FromUrlInvalid);
                return;
            }

            var fromHost = fromUri.Host;
            if (fromHost != "www.cancer.ca")
            {
                SetStatus(RedirectStatus.FromUrlHostInvalid, fromHost);
                return;
            }
            var subpath = AppUtils.GetItemStem(FromUrl);
            if (AppUtils.IsCancerCaSubpath(subpath))
            {
                SetStatus(RedirectStatus.FromUrlUnexpected, "Cancer info page");
                return;
            }

            var toUri = AppUtils.GetUri(ToUrl);
            if (AppUtils.IsNullOrEmpty(toUri)
                || toUri.Query.Contains("sc_itemid"))
            {
                SetStatus(RedirectStatus.ToUrlInvalid);
                return;
            }

            var adjustedToUri = new UriBuilder(toUri);
            switch (adjustedToUri.Host)
            {
                case "action.cancer.ca":
                    SetStatus(RedirectStatus.OKActionRedirect);
                    break;

                case "cduat.cancer.ca":
                    adjustedToUri.Host = "action.cancer.ca";
                    SetStatus(RedirectStatus.OKActionRedirect);
                    break;

                case "support.cancer.ca":
                    SetStatus(RedirectStatus.OKOtherRedirect);
                    break;

                default:
                    SetStatus(RedirectStatus.ToUrlHostInvalid, adjustedToUri.Host);
                    return;
            }

            var adjustedToUrl = AppUtils.FormatUrl(adjustedToUri);
            if (ToUrl != adjustedToUrl)
                AdjustedToUrl = adjustedToUrl;
        }

        public void SetStatus(RedirectStatus status, string statusText = null)
        {
            Status = status;
            StatusText = statusText;
        }
    }
}
