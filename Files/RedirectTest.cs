﻿using System;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files
{
    public class RedirectTest 
    {
        public string Source { get; set; }

        [Description("Source Row")]
        public int SourceRow { get; set; }

        [Description("SC72 Url")]
        public string OldUrl { get; set; }

        [Description("SC93 Url")]
        public string NewUrl { get; set; }

        [Description("SC93 Redirect Url")]
        public string RedirectUrl { get; set; }

        [Description("Redirect Status")]
        public string RedirectStatus { get; set; }

        [Description("Status")]
        public RedirectTesMatchStatus Status { get; set; }


        public void ComputeStatus(bool IsMatch)
        {
            if (IsMatch)
            {
                SetStatus(RedirectTesMatchStatus.Match, "Redirect Successful");
            }
            else
            {
                SetStatus(RedirectTesMatchStatus.NotMatch, "Redirect Fail");
            }
        }

        public void SetStatus(RedirectTesMatchStatus status, string statusText = null)
        {
            Status = status;
            RedirectStatus = statusText;
        }
    }
}
