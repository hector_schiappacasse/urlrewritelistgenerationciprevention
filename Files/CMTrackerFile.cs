﻿using OfficeOpenXml;
using System.Collections.Generic;
using static UrlRewriterListGenerator.Constants;
using Xls = UrlRewriterListGenerator.Constants.ContentMigrationTrackerXls;

namespace UrlRewriterListGenerator.Files
{
    public class CMTrackerFile
    {
        public CMTrackerFile(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public List<CMTrackerRedirect> GetAllRedirects()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.Redirects))
            {
                var headerRow = ws.Dimension.Start.Row;
                var topRow = headerRow + 1;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(headerRow);
                var includedCol = headerCells.FindByText(Xls.ColumnNames.Included).GetCol();
                var fromUrlCol = headerCells.FindByText(Xls.ColumnNames.FromUrl).GetCol();
                var toUrlCol = headerCells.FindByText(Xls.ColumnNames.ToUrl).GetCol();

                var redirects = new List<CMTrackerRedirect>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    var redirect = new CMTrackerRedirect
                    {
                        Source = RedirectSources.ContentMigrationTracker,
                        SourceRow = row,
                        IsIncluded = ws.Cells[row, includedCol].Text == "1",
                        FromUrl = ws.Cells[row, fromUrlCol].Text,
                        ToUrl = ws.Cells[row, toUrlCol].Text
                    };
                    redirect.ComputeStatus();
                    redirects.Add(redirect);
                }
                return redirects;
            }
        }
    }
}
