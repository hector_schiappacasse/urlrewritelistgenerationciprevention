﻿using System;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files
{
    public class Redirect
    {
        private static readonly string emptyText = "-";

        public string Source { get; set; }

        [Description("Src Row")]
        public int SourceRow { get; set; }

        public RedirectStatus Status { get; set; }

        public string StatusText { get; set; }

        [Description("RW Map")]
        public string RewriteMapName { get; set; }

        [Description("RW Key")]
        public string RedirectKey { get; set; }

        [Description("RW Value")]
        public string RedirectValue { get; set; }

        [Description("From Url")]
        public string FromUrl { get; set; }

        [Description("To Url")]
        public string ToUrl { get; set; }

        public void SetStatus(RedirectStatus status, string statusText = null)
        {
            Status = status;
            StatusText = statusText;
        }

        #region Uris

        private Uri FromUri => AppUtils.GetUri(FromUrl);
        private Uri ToUri => AppUtils.GetUri(ToUrl);

        #endregion

        public override string ToString()
        {
            return $@"Src={Source} #={SourceRow} Status={Status} Map={RewriteMapName} Key={RedirectKey} Value={RedirectValue} From={FromUrl} To={ToUrl}";
        }
    }
}
