﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files
{
    public class PageSource
    {
        public string Source { get; set; }

        [Description("Source Row")]
        public int SourceRow { get; set; }

        [Description("Status")]
        public PageMatchStatus Status { get; set; }

        [Description("Status Text")]
        public string StatusText { get; set; }

        [Description("Item Path")]
        public string ItemPath { get; set; }

        [Description("Item ID")]
        public string ItemId { get; set; }

        [Description("English Title")]
        public string EnglishTitle { get; set; }

        private string _fromUrl;
        [Description("English Url")]
        public string EnglishUrl
        {
            get
            {
                return "/en/" + AppUtils.GetItemStem(_fromUrl);
            }
            set { _fromUrl = value; }
        }

        [Description("French Title")]
        public string FrenchTitle { get; set; }

        [Description("French Url")]
        public string FrenchUrl { get; set; }

        public void ComputeStatus(List<Redirect> redirectUrls)
        {

        }

        public void ComputeStatus(List<string> redirectUrls)
        {
            var url = EnglishUrl;    //   AppUtils.GetItemStem(EnglishUrl);

            if (string.IsNullOrWhiteSpace(url))
            {
                SetStatus(PageMatchStatus.NotApplicable, "Page Url is missing");
                return;
            }

            var uri = AppUtils.GetUri(url);
            if (AppUtils.IsNullOrEmpty(uri))
            {
                SetStatus(PageMatchStatus.NotApplicable, "Page Url Invalid");
                return;
            }

            var host = uri.Host;
            if (host != "www.cancer.ca")
            {
                SetStatus(PageMatchStatus.NotApplicable, host);
                return;
            }

            var subpath = AppUtils.GetItemStem(url);
            if (!AppUtils.IsCancerCaMatchSubpath(subpath))
            {
                SetStatus(PageMatchStatus.NotApplicable, "Not info or prevention page");
                return;
            }

            bool is_match = redirectUrls.Any(x => AppUtils.GetItemStem(x) == subpath);
            var status =  is_match? PageMatchStatus.Match : PageMatchStatus.NotMatch;
            SetStatus(status);
        }

        private void SetStatus(PageMatchStatus status, string statusText = null)
        {
            Status = status;
            StatusText = statusText;
        }

    }
}
