﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using Xls = UrlRewriterListGenerator.Constants.RedirectTestXLs;

namespace UrlRewriterListGenerator.Files
{
    public class RedirectTestFile
    {
        public RedirectTestFile(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public List<RedirectTest> GetAllRedirects()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.Redirects))
            {
                var headerRow = ws.Dimension.Start.Row;
                var topRow = headerRow + 1;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(headerRow);
                var sc72 = headerCells.FindByText(Xls.ColumnNames.SC72).GetCol();
                var sc93 = headerCells.FindByText(Xls.ColumnNames.SC93).GetCol();

                var pageList = new List<RedirectTest>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    var page = new RedirectTest
                    {
                        SourceRow = row,
                        OldUrl = ws.Cells[row, sc72].Text,
                        NewUrl = ws.Cells[row, sc93].Text
                    };
                    //page.ComputeStatus(Redirects);
                    pageList.Add(page);
                }
                return pageList;
            }
        }

        public void UpdateStatus(List<RedirectTest> pageList)
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.Redirects))
            {
                var headerRow = ws.Dimension.Start.Row;
                var topRow = headerRow + 1;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(headerRow);
                var redirectStatus = headerCells.FindByText(Xls.ColumnNames.RedirectStatus).GetCol();

                pageList.ForEach(x =>
                {
                    ws.Cells[x.SourceRow, redirectStatus].Value = x.RedirectStatus;
                });
                Package.Save();
                //Package.SaveAs(Package.File);
            }
        }

    }

    public enum RedirectTesMatchStatus
    {
        Match = 200,
        NotApplicable,
        NotMatch = 0
    }

}
