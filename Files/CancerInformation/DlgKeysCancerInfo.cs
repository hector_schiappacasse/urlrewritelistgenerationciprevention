﻿using System;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class DlgKeysCancerInfo
    {
        public string Source { get; set; }

        [Description("Source Row")]
        public int SourceRow { get; set; }

        [Description("Item ID")]
        public string ItemId { get; set; }

        [Description("Item Path")]
        public string ContentPath { get; set; }

        [Description("Ezd ID")]
        public string EzdId { get; set; }

        [Description("Toc ID")]
        public string TocId { get; set; }

        [Description("Cce ID")]
        public string CceId { get; set; }

        [Description("Redirect Status")]
        public string RedirectStatus { get; set; }

        [Description("Status")]
        public DlgKeysStatus Status { get; set; }


        public void ComputeStatus()
        {
            if (String.IsNullOrEmpty(this.ItemId)
                || String.IsNullOrEmpty(this.CceId)
                || String.IsNullOrEmpty(this.TocId)
            )
            {
                SetStatus(DlgKeysStatus.Fail, "Value is missing");
            }
            else
            {
                SetStatus(DlgKeysStatus.Success, "Record is valid");
            }
        }

        public void SetStatus(DlgKeysStatus status, string statusText = null)
        {
            Status = status;
            RedirectStatus = statusText;
        }
    }
}
