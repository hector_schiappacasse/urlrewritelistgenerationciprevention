﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using Xls = UrlRewriterListGenerator.Constants.DlgKeysCancerInfoXLs;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class DlgKeysCancerInfoFile
    {
        public DlgKeysCancerInfoFile(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public List<DlgKeysCancerInfo> GetAllKeys()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.DlgKeysCancerInfo))
            {
                var headerRow = ws.Dimension.Start.Row;
                var topRow = headerRow + 1;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(headerRow);

                var itemPath = headerCells.FindByText(Xls.ColumnNames.ContentPath).GetCol();
                var itemId = headerCells.FindByText(Xls.ColumnNames.ItemId).GetCol();
                var cceId = headerCells.FindByText(Xls.ColumnNames.CceId).GetCol();
                var tocId = headerCells.FindByText(Xls.ColumnNames.TocId).GetCol();

                var pageList = new List<DlgKeysCancerInfo>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    var page = new DlgKeysCancerInfo
                    {
                        SourceRow = row,
                        ContentPath = ws.Cells[row, itemPath].Text,
                        ItemId= ws.Cells[row, itemId].Text,
                        CceId= ws.Cells[row, cceId].Text,
                        TocId= ws.Cells[row, tocId].Text,
                    };
                    page.ComputeStatus();
                    pageList.Add(page);
                }
                return pageList;
            }
        }

    }
    public enum DlgKeysStatus
    {
        Success = 200,
        NotApplicable,
        Fail = 0
    }

}
