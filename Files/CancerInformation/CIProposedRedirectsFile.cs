﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using Xls = UrlRewriterListGenerator.Constants.CIProposedRedirectsXLs;


namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class CIProposedRedirectsFile
    {
        public CIProposedRedirectsFile(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public List<CIProposedRedirects> GetAllUrls()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.CIProposedRedirects))
            {
                var headerRow = ws.Dimension.Start.Row;
                var topRow = headerRow + 1;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(headerRow);

                var fromUrl = headerCells.FindByText(Xls.ColumnNames.FromUrl).GetCol();
                var toUrl = headerCells.FindByText(Xls.ColumnNames.ToUrl).GetCol();

                var pageList = new List<CIProposedRedirects>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    string fromUrlText = ws.Cells[row, fromUrl].Text;
                    string toUrlText = ws.Cells[row, toUrl].Text;
                    var page = new CIProposedRedirects
                    {
                        SourceCIRow = row,
                        FromUrl = ws.Cells[row, fromUrl].Text,
                        ToUrl = ws.Cells[row, toUrl].Text,
                        //FromUrl = fromUrlText.Contains("/en/") ? fromUrlText:"N/A" ,
                        //ToUrl = toUrlText.Contains("/en/") ? toUrlText : "N/A",
                    };
                    pageList.Add(page);
                }
                return pageList;
            }
        }
    }

    public enum CIredirectStatus
    {
        CI_NotIn_SC9 = 200,
        CI_NotIn_SC7 = 300,
        NotApplicable,
        Found = 0,
        SC72_NotIn_CI = 10
    }

}
