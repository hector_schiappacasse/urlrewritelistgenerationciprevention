﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using Xls = UrlRewriterListGenerator.Constants.CurrentPageNamesXLs;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class CurrentPageNameFile
    {
        public CurrentPageNameFile(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public List<CurrentPageName> GetAllKeys()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.CurrentPageNames))
            {
                var headerRow = ws.Dimension.Start.Row;
                var topRow = headerRow + 1;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(headerRow);

                var cceTocIdEn = headerCells.FindByText(Xls.ColumnNames.CceTocIdEn).GetCol();
                var ezdIdEn = headerCells.FindByText(Xls.ColumnNames.EzdIdEn).GetCol();
                var cceTocIdFr = headerCells.FindByText(Xls.ColumnNames.CceTocIdFr).GetCol();
                var ezdIdFr = headerCells.FindByText(Xls.ColumnNames.EzdIdFr).GetCol();

                var pageList = new List<CurrentPageName>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    var pageEn = new CurrentPageName { SourceRow = row };
                    pageEn.ComputeEzId(ws.Cells[row, ezdIdEn].Text);
                    pageEn.ComputeCceIdToCId(ws.Cells[row, cceTocIdEn].Text);
                    pageEn.ComputeStatus();
                    pageList.Add(pageEn);

                    var pageFr = new CurrentPageName { SourceRow = row };
                    pageFr.ComputeEzId(ws.Cells[row, ezdIdFr].Text);
                    pageFr.ComputeCceIdToCId(ws.Cells[row, cceTocIdFr].Text);
                    pageFr.ComputeStatus();
                    pageList.Add(pageFr);
                }
                return pageList;
            }
        }

    }
    public enum CurrentPageNameStatus
    {
        Success = 200,
        NotApplicable,
        Fail = 0
    }

}
