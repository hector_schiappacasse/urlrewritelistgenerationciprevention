﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using Xls = UrlRewriterListGenerator.Constants.CamsPageXLs;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class CamsPageFile
    {
        public CamsPageFile(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public List<CamsPage> GetAllUrls()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.CamsPages))
            {
                var headerRow = ws.Dimension.Start.Row;
                var topRow = headerRow + 1;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(headerRow);

                var itemId = headerCells.FindByText(Xls.ColumnNames.ContentItemID).GetCol();
                var itemPath = headerCells.FindByText(Xls.ColumnNames.ContentPath).GetCol();
                var itemLang = headerCells.FindByText(Xls.ColumnNames.ItemLanguage).GetCol();
                var ezdId = headerCells.FindByText(Xls.ColumnNames.EzdId).GetCol();

                var pageList = new List<CamsPage>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    var page = new CamsPage
                    {
                        SourceRow = row,
                        ContentItemID = ws.Cells[row, itemId].Text,
                        ContentPath = ws.Cells[row, itemPath].Text,
                        ItemLanguage = ws.Cells[row, itemLang].Text,
                        EzdId = ws.Cells[row, ezdId].Text
                    };
                    pageList.Add(page);
                }
                return pageList;
            }
        }
    }

}
