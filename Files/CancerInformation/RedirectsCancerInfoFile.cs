﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using XLs = UrlRewriterListGenerator.Constants.RedirectCancerInfoXLs;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class RedirectsCancerInfo
    {
        public RedirectsCancerInfo(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public void AddRedirectsWorksheet(string worksheetName, IEnumerable<RedirectCancerInfo> redirects)
        {
            var ws = Package.GetOrAddWorksheet(worksheetName);

            ws.Cells.Clear();

            var titleRow = 1;
            var headerRow = titleRow + 2;
            var topRow = headerRow + 1;

            ws.SetTitleCellText(titleRow, 1, worksheetName);
            ws.Row(headerRow).Height = 45;

            using (var range = ws.Cells[headerRow, 1].LoadFromCollection(redirects, true, TableStyles.Medium2))
            {
                range.SetRangeBorders();
                range.Style.WrapText = true;
                ws.View.FreezePanes(topRow, 1);
            }

            var col = 0;
            var statusCol = ws.InitColumn(headerRow, ++col, width: 20.0);
            var statusTextCol = ws.InitColumn(headerRow, ++col, width: 5.0);
            var srcCiCol = ws.InitColumn(headerRow, ++col, width: 7.0);
            var srcOldCol = ws.InitColumn(headerRow, ++col, width: 7.0);
            var srcNewCol = ws.InitColumn(headerRow, ++col, width: 7.0);
            var langCol = ws.InitColumn(headerRow, ++col, width: 5.0);
            var fromUrlCol = ws.InitColumn(headerRow, ++col, width: 100.0, wrapText: true);
            var toUrlCol = ws.InitColumn(headerRow, ++col, width: 100.0, wrapText: true);
            var fromUrlCICol = ws.InitColumn(headerRow, ++col, width: 100.0, wrapText: true);
            var toUrlCICol = ws.InitColumn(headerRow, ++col, width: 100.0, wrapText: true);
        }

        public void AddRedirectsWorksheet(string worksheetName, IEnumerable<CIProposedRedirects> redirects)
        {
            var ws = Package.GetOrAddWorksheet(worksheetName);

            ws.Cells.Clear();

            var titleRow = 1;
            var headerRow = titleRow + 2;
            var topRow = headerRow + 1;

            ws.SetTitleCellText(titleRow, 1, worksheetName);
            ws.Row(headerRow).Height = 45;

            using (var range = ws.Cells[headerRow, 1].LoadFromCollection(redirects, true, TableStyles.Medium2))
            {
                range.SetRangeBorders();
                range.Style.WrapText = true;
                ws.View.FreezePanes(topRow, 1);
            }

            //using (var range = ws.Cells[headerRow, 3, redirects.ToList().Count, 3])
            //{
            //    foreach (var cell in range)
            //    {
            //        cell.Hyperlink = new Uri("http://www.google.com");
            //        cell.Value = "Click me!";
            //    }
            //}

            var col = 0;
            var sourceRowCiCol = ws.InitColumn(headerRow, ++col, width: 7.0);
            //var sourceRowSc7Col = ws.InitColumn(headerRow, ++col, width: 7.0);
            var statusSc7Col = ws.InitColumn(headerRow, ++col, width: 14.0);
            var fromUrlCol = ws.InitColumn(headerRow, ++col, width: 100.0);
            var fromUrlHlCol = ws.InitColumn(headerRow, ++col, width: 100.0);
            var statusSc9Col = ws.InitColumn(headerRow, ++col, width: 14.0, wrapText: true);
            var toUrlCol = ws.InitColumn(headerRow, ++col, width: 100.0, wrapText: true);
            var toUrlHlCol = ws.InitColumn(headerRow, ++col, width: 100.0, wrapText: true);
        }

        public void AddRedirectsWorksheet(string worksheetName, IEnumerable<PageSource> pageList)
        {
            var ws = Package.GetOrAddWorksheet(worksheetName);

            ws.Cells.Clear();

            var titleRow = 1;
            var headerRow = titleRow + 2;
            var topRow = headerRow + 1;

            ws.SetTitleCellText(titleRow, 1, worksheetName);

            using (var range = ws.Cells[headerRow, 1].LoadFromCollection(pageList, true, TableStyles.Medium2))
            {
                range.SetRangeBorders();
                ws.View.FreezePanes(topRow, 1);
            }

            var col = 0;
            var sourceCol = ws.InitColumn(headerRow, ++col, width: 10.0);
            var sourceRowCol = ws.InitColumn(headerRow, ++col, width: 6.0);
            var statusCol = ws.InitColumn(headerRow, ++col, width: 13.0, wrapText: true);
            var statusTextCol = ws.InitColumn(headerRow, ++col, width: 24.0, wrapText: true);
            var itemPathCol = ws.InitColumn(headerRow, ++col, width: 24.0, wrapText: true);
            var englishTitleCol = ws.InitColumn(headerRow, ++col, width: 26.0, wrapText: true);
            var englishUrlCol = ws.InitColumn(headerRow, ++col, width: 30.0, wrapText: true);
            var frenchTitleCol = ws.InitColumn(headerRow, ++col, width: 26.0, wrapText: true);
            var frenchUrlCol = ws.InitColumn(headerRow, ++col, width: 30.0, wrapText: true);
        }

    }
}
