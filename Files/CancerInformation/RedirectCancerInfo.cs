﻿using System;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class RedirectCancerInfo
    {
        private RedirectCancerInfoStatus _status = RedirectCancerInfoStatus.NotApplicable;

        public RedirectCancerInfoStatus Status
        {
            get
            {
                if(String.IsNullOrEmpty(FromUrl) && String.IsNullOrEmpty(ToUrl))
                    return RedirectCancerInfoStatus.NotFoundSC;
                else if (Language=="en" && (String.IsNullOrEmpty(FromUrlCI) || String.IsNullOrEmpty(ToUrlCI)))
                    return RedirectCancerInfoStatus.NotFoundCI;
                else if (Language == "en" && FromUrlCI != FromUrl)
                    return RedirectCancerInfoStatus.CINotMatchSC72;
                else if(String.IsNullOrEmpty(ToUrl))
                    return RedirectCancerInfoStatus.NotFoundSC93;
                else if(String.IsNullOrEmpty(FromUrl))
                    return RedirectCancerInfoStatus.NotFoundSC72;
                else
                    return RedirectCancerInfoStatus.Success;
            }
        }

        public string StatusText { get; set; }

        [Description("Source Row Old")]
        public int SourceRowSc72 { get; internal set; }

        [Description("Source Row New")]
        public int SourceRowSc93 { get; internal set; }

        [Description("Source Row CI")]
        public int SourceCIRow { get; set; }

        [Description("Language")]
        public string Language { get; set; }

        private string _fromUrl;
        [Description("From Url")]
        public string FromUrl
        {
            get
            {
                Uri fromUri = null;
                if (Uri.TryCreate(_fromUrl, UriKind.RelativeOrAbsolute, out fromUri) && fromUri.IsAbsoluteUri)
                //if (Uri.IsWellFormedUriString(_fromUrl, UriKind.RelativeOrAbsolute))
                {
                    return AppUtils.GetAbsolutePath(_fromUrl);
                }
                else
                {
                    if (_fromUrl.EndsWith("/")) _fromUrl = _fromUrl.Substring(0, _fromUrl.Length - 1);
                    return _fromUrl;
                }
            }
            set { _fromUrl = value; }
        }

        [Description("To Url")]
        public string ToUrl { get; set; }

        private string _fromUrlCI;
        [Description("From Url from CI")]
        public string FromUrlCI
        {
            get
            {
                Uri fromUri = null;
                if (Uri.TryCreate(_fromUrlCI, UriKind.RelativeOrAbsolute, out fromUri) && fromUri.IsAbsoluteUri)
                {
                    return AppUtils.GetAbsolutePath(_fromUrlCI);
                }
                else
                {
                    if (_fromUrlCI.EndsWith("/")) _fromUrlCI = _fromUrlCI.Substring(0, _fromUrlCI.Length - 1);
                    return _fromUrlCI;
                }
            }
            set { _fromUrlCI = value; }
        }

        [Description("To Url from CI")]
        public string ToUrlCI { get; set; }

        //public override string ToString()
        //{
        //    return $@"Src={Source} #={SourceRow} Status={Status} Map={RewriteMapName} Key={RedirectKey} Value={RedirectValue} From={FromUrl} To={ToUrl}";
        //}
    }

    public enum RedirectCancerInfoStatus
    {
        NotApplicable,
        Fail = 0,
        NotFoundSC72 = 100,
        NotFoundSC93 = 200,
        NotFoundCI = 300,
        NotFoundSC = 400,
        CINotMatchSC72 = 500,
        SuccessCISC = 800,
        Success = 900,
    }

}
