﻿using System;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class CIProposedRedirects
    {
        [Description("Source CI Row")]
        public int SourceCIRow { get; set; }

        //[Description("Source SC7 Row")]
        //public int SourceSC7Row { get; set; }

        [Description("Status From URL")]
        public CIredirectStatus StatusSC7 { get; set; }

        private string _fromUrl;
        [Description("From URL")]
        public string FromUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_fromUrl))
                {
                    return string.Empty;
                }
                else
                {
                    if (_fromUrl.EndsWith("/")) _fromUrl = _fromUrl.Substring(0, _fromUrl.Length - 1).Trim().ToLower().Replace(' ', '-');
                    return _fromUrl;
                }
            }
            set { _fromUrl = value; }

        }

        [Description("From URL Hyperlink")]
        public string FromUrlHL{get;set;}

        [Description("Status To URL")]
        public CIredirectStatus StatusSC9 { get; set; }

        private string _toUrl;
        [Description("To URL")]
        public string ToUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_toUrl))
                {
                    return string.Empty;
                }
                else
                {
                    if (_toUrl.EndsWith("/")) _toUrl = _toUrl.Substring(0, _toUrl.Length - 1).Trim().ToLower().Replace(' ', '-');
                    return _toUrl;
                }
            }
            set { _toUrl = value; }
        }

        [Description("To URL Hyperlink")]
        public string ToUrlHL{get;set;}
    }
}
