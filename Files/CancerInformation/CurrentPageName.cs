﻿using System;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class CurrentPageName
    {
        public string Source { get; set; }

        [Description("Source Row")]
        public int SourceRow { get; set; }

        [Description("Item Language")]
        public string ItemLanguage { get; set; }

        [Description("Ezd ID")]
        public string EzdId { get; set; }

        [Description("Toc ID")]
        public string TocId { get; set; }

        [Description("Cce ID")]
        public string CceId { get; set; }

        [Description("Status Text")]
        public string StatusText { get; set; }

        [Description("Status")]
        public CurrentPageNameStatus Status { get; set; }


        public void ComputeStatus()
        {
            if (String.IsNullOrEmpty(this.EzdId)
                || String.IsNullOrEmpty(this.CceId)
                || String.IsNullOrEmpty(this.TocId)
                || String.IsNullOrEmpty(this.ItemLanguage)
            )
            {
                SetStatus(CurrentPageNameStatus.Fail, "Value is missing");
            }
            else
            {
                SetStatus(CurrentPageNameStatus.Success, "Record is valid");
            }
        }

        public void SetStatus(CurrentPageNameStatus status, string statusText = null)
        {
            Status = status;
            StatusText = statusText;
        }

        internal void ComputeCceIdToCId(string cceTocId)
        {
            if (String.IsNullOrEmpty(cceTocId)) return;
            char[] delim = new char[] { '_' };
            string[] cceTocIdArray = cceTocId.Split(delim);
            int count = cceTocIdArray.Length;
            if (count > 3)
            {
                CceId = cceTocIdArray[count - 2];
                TocId = cceTocIdArray[count - 3];
            }
        }

        internal void ComputeEzId(string ezdId)
        {
            if (String.IsNullOrEmpty(ezdId)) return;
            char[] delim = new char[] { '-' };
            string[] ezdIdrray = ezdId.Split(delim, 3);
            ItemLanguage = ezdIdrray[0];
            EzdId = ezdIdrray[2];
        }

    }
}
