﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace UrlRewriterListGenerator.Files.CancerInfomation
{
    public class CamsPage
    {
        public string Source { get; set; }

        [Description("Source Row")]
        public int SourceRow { get; set; }

        [Description("Item ID")]
        public string ContentItemID { get; set; }

        [Description("Item Path")]
        public string ContentPath { get; set; }

        [Description("Item Language")]
        public string ItemLanguage { get; set; }

        [Description("Ezd ID")]
        public string EzdId { get; set; }

        [Description("SC93 URL Path")]
        public string Sc93URLPath
        {
            get
            {
                var toUri = AppUtils.GetAbsolutePath(this.ContentPath, this.ItemLanguage);
                if (String.IsNullOrEmpty(toUri))
                {
                    SetStatus(RedirectStatus.ToUrlInvalid);
                    return string.Empty;
                }
                else
                {
                    SetStatus(RedirectStatus.OK);
                    return toUri;
                }

            }
        }

        [Description("Status Text")]
        public string StatusText { get; set; }

        [Description("Status")]
        public RedirectStatus Status { get; set; }


        public void SetStatus(RedirectStatus status, string statusText = null)
        {
            Status = status;
            StatusText = statusText;
        }


    }

}
