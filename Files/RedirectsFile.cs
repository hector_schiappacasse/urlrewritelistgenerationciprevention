﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using System.Collections.Generic;
using System.Drawing;

namespace UrlRewriterListGenerator.Files
{
    public class RedirectsFile
    {
        public RedirectsFile(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public void AddRedirectsWorksheet(string worksheetName, IEnumerable<CMTrackerRedirect> redirects)
        {
            var ws = Package.GetOrAddWorksheet(worksheetName);

            ws.Cells.Clear();

            var titleRow = 1;
            var headerRow = titleRow + 2;
            var topRow = headerRow + 1;

            ws.SetTitleCellText(titleRow, 1, worksheetName);

            using (var range = ws.Cells[headerRow, 1].LoadFromCollection(redirects, true, TableStyles.Medium2))
            {
                range.SetRangeBorders();
                ws.View.FreezePanes(topRow, 1);
            }

            var col = 0;
            var sourceCol = ws.InitColumn(headerRow, ++col, width: 14.0);
            var sourceRowCol = ws.InitColumn(headerRow, ++col, width: 15.0);
            var statusCol = ws.InitColumn(headerRow, ++col, width: 20.0);
            var statusTextCol = ws.InitColumn(headerRow, ++col, width: 25.0);
            var isIncludedCol = ws.InitColumn(headerRow, ++col, width: 12.0);
            var fromUrlCol = ws.InitColumn(headerRow, ++col, width: 50.0, wrapText: true);
            var toUrlCol = ws.InitColumn(headerRow, ++col, width: 50.0, wrapText: true);
            var adjustedToUrlCol = ws.InitColumn(headerRow, ++col, width: 50.0, wrapText: true);
        }

        public void AddRedirectsWorksheet(string worksheetName, IEnumerable<Redirect> redirects)
        {
            var ws = Package.GetOrAddWorksheet(worksheetName);

            ws.Cells.Clear();

            var titleRow = 1;
            var headerRow = titleRow + 2;
            var topRow = headerRow + 1;

            ws.SetTitleCellText(titleRow, 1, worksheetName);

            using (var range = ws.Cells[headerRow, 1].LoadFromCollection(redirects, true, TableStyles.Medium2))
            {
                range.SetRangeBorders();
                ws.View.FreezePanes(topRow, 1);
            }

            var col = 0;
            var sourceCol = ws.InitColumn(headerRow, ++col, width: 14.0);
            var sourceRowCol = ws.InitColumn(headerRow, ++col, width: 6.0);
            var statusCol = ws.InitColumn(headerRow, ++col, width: 20.0);
            var statusTextCol = ws.InitColumn(headerRow, ++col, width: 25.0);
            var rewriteMapCol = ws.InitColumn(headerRow, ++col, width: 15.0, wrapText: true);
            var rewriteKeyCol = ws.InitColumn(headerRow, ++col, width: 36.0, wrapText: true);
            var rewriteValueCol = ws.InitColumn(headerRow, ++col, width: 36.0, wrapText: true);
            var fromUrlCol = ws.InitColumn(headerRow, ++col, width: 40.0, wrapText: true);
            var toUrlCol = ws.InitColumn(headerRow, ++col, width: 40.0, wrapText: true);
        }

        public void AddRedirectsWorksheet(string worksheetName, IEnumerable<PageSource> pageList)
        {
            var ws = Package.GetOrAddWorksheet(worksheetName);

            ws.Cells.Clear();

            var titleRow = 1;
            var headerRow = titleRow + 2;
            var topRow = headerRow + 1;

            ws.SetTitleCellText(titleRow, 1, worksheetName);

            using (var range = ws.Cells[headerRow, 1].LoadFromCollection(pageList, true, TableStyles.Medium2))
            {
                range.SetRangeBorders();
                ws.View.FreezePanes(topRow, 1);
            }

            var col = 0;
            var sourceCol = ws.InitColumn(headerRow, ++col, width: 10.0);
            var sourceRowCol = ws.InitColumn(headerRow, ++col, width: 6.0);
            var statusCol = ws.InitColumn(headerRow, ++col, width: 13.0, wrapText: true);
            var statusTextCol = ws.InitColumn(headerRow, ++col, width: 24.0, wrapText: true);
            var itemPathCol = ws.InitColumn(headerRow, ++col, width: 24.0, wrapText: true);
            var englishTitleCol = ws.InitColumn(headerRow, ++col, width: 26.0, wrapText: true);
            var englishUrlCol = ws.InitColumn(headerRow, ++col, width: 30.0, wrapText: true);
            var frenchTitleCol = ws.InitColumn(headerRow, ++col, width: 26.0, wrapText: true);
            var frenchUrlCol = ws.InitColumn(headerRow, ++col, width: 30.0, wrapText: true);
        }
    }
}
