﻿using System.ComponentModel;

namespace UrlRewriterListGenerator.Files
{
    public enum RedirectStatus
    {
        None = 0,

        Excluded = 100,
        FromUrlMissing,
        FromUrlInvalid,
        FromUrlHostInvalid,
        FromUrlUnexpected,
        FromUrlDuplicate,
        ToUrlMissing,
        ToUrlInvalid,
        ToUrlHostInvalid,

        OK = 200,
        OKActionRedirect,
        OKOtherRedirect
    }

    public interface IRedirectBase
    {
        string Source { get; set; }
        int SourceRow { get; set; }
        RedirectStatus Status { get; set; }
        string StatusText { get; set; }
        string FromUrl { get; set; }
        string ToUrl { get; set; }
        string AdjustedToUrl { get; set; }

        void SetStatus(RedirectStatus status, string statusText = null);
    }
}