﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using static UrlRewriterListGenerator.Constants;
using Xls = UrlRewriterListGenerator.Constants.PageListXls;

namespace UrlRewriterListGenerator.Files
{
    public class PageListFile
    {
        public PageListFile(ExcelPackage package, IEnumerable<Redirect> redirects)
        {
            Package = package;
            Redirects = redirects.Where(x => x.Status == RedirectStatus.OKActionRedirect).Select(x=>x.FromUrl).ToList();
        }

        public ExcelPackage Package { get; }
        public List<string> Redirects { get; }

        public List<PageSource> GetAllSourcePages()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.PageList))
            {
                var headerRow = ws.Dimension.Start.Row + 4;
                var topRow = headerRow + 1;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(headerRow);
                var itemId = headerCells.FindByText(Xls.ColumnNames.ItemId).GetCol();
                var itemPath = headerCells.FindByText(Xls.ColumnNames.ItemPath).GetCol();
                var englishTitle = headerCells.FindByText(Xls.ColumnNames.EnglishTitle).GetCol();
                var englishUrl = headerCells.FindByText(Xls.ColumnNames.EnglishUrl).GetCol();
                var frenchTitle = headerCells.FindByText(Xls.ColumnNames.FrenchTitle).GetCol();
                var frenchUrl = headerCells.FindByText(Xls.ColumnNames.FrenchUrl).GetCol();

                var pageList = new List<PageSource>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    var page = new PageSource
                    {
                        Source = RedirectSources.PageListSource,
                        SourceRow = row,
                        ItemId = ws.Cells[row, itemId].Text,
                        ItemPath = ws.Cells[row, itemPath].Text,
                        EnglishTitle = ws.Cells[row, englishTitle].Text,
                        EnglishUrl = ws.Cells[row, englishUrl].Text,
                        FrenchTitle = ws.Cells[row, frenchTitle].Text,
                        FrenchUrl = ws.Cells[row, frenchUrl].Text
                    };
                    page.ComputeStatus(Redirects);
                    pageList.Add(page);
                }
                return pageList;
            }
        }
    }

    public enum PageMatchStatus
    {
        Match = 200,
        NotApplicable,
        NotMatch = 0
    }

}
