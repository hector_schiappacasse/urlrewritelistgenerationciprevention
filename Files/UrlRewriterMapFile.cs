﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UrlRewriterListGenerator.Files;

namespace UrlRewriterListGenerator.Files
{
    public static class UrlRewriterMapFile
    {
        public static void CreateFile(string path, IEnumerable<Redirect> redirects)
        {
            var rewriteMapsElement =
                new XElement("rewriteMaps",
                from redirect in redirects
                group redirect by redirect.RewriteMapName into mapGroup
                orderby mapGroup.Key
                select new XElement("rewriteMap",
                    new XAttribute("name", mapGroup.Key),
                    from mapRedirect in mapGroup
                    orderby mapRedirect.RedirectKey
                    select new XElement("add",
                        new XAttribute("key", mapRedirect.RedirectKey),
                        new XAttribute("value", mapRedirect.RedirectValue ?? string.Empty))));

            rewriteMapsElement.Save(path);
        }
    }
}
