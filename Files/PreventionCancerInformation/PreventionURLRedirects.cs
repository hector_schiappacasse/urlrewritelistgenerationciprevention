﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UrlRewriterListGenerator.Files.PreventionCancerInformation
{
    public class PreventionURLRedirects
    {
        [Description("Old page title - EN")]
        public string OldPageTitleEn { get; set; }

        //[Browsable(false)]
        //public int SourcePRow { get; set; }

        [Description("Status From URL")]
        public PRredirectStatus StatusSC7 { get; set; }

         private string _fromUrl;
        [Description("Old URL - EN")]
        public string FromUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_fromUrl))
                {
                    return string.Empty;
                }
                else
                {
                    if (_fromUrl.EndsWith("/")) _fromUrl = _fromUrl.Substring(0, _fromUrl.Length - 1).Trim().ToLower().Replace(' ', '-');
                    return _fromUrl;
                }
            }
            set { _fromUrl = value; }

        }


        [Description("Old page title - FR")]
        public string OldPageTitleFr { get; set; }

        [Description("New page title - EN")]
        public string NewPageTitleEn { get; set; }

        //[Description("From URL Hyperlink")]
        //public string FromUrlHL { get; set; }

        [Description("Status New URL")]
        public PRredirectStatus StatusSC9 { get; set; }

        private string _toUrl;
        [Description("New URL")]
        public string ToUrl
        {
            get
            {
                if (String.IsNullOrEmpty(_toUrl))
                {
                    return string.Empty;
                }
                else
                {
                    if (_toUrl.EndsWith("/")) _toUrl = _toUrl.Substring(0, _toUrl.Length - 1).Trim().ToLower().Replace(' ', '-');
                    return _toUrl;
                }
            }
            set { _toUrl = value; }
        }
        private string _OldUrlPath;
        [Description("Old URL Path")]
        public string OldUrlPath {
            get
            {
                if (String.IsNullOrEmpty(_OldUrlPath))
                {
                    return string.Empty;
                }
                else
                {
                    if (_OldUrlPath.Replace("\n", "").EndsWith("/")) _OldUrlPath = _OldUrlPath.Substring(0, _OldUrlPath.Length - 1).Trim().ToLower().Replace(' ', '-').Replace("https://www.cancer.ca/en","");
                    else
                        _OldUrlPath = _OldUrlPath.Replace("https://www.cancer.ca/en", "");
                    return _OldUrlPath;
                }
            }
            set { _OldUrlPath = value; }
        }

        private string _NewUrlPath;
        [Description("New URL Path")]
        public string NewUrlPath
        {
            get
            {
                if (String.IsNullOrEmpty(_NewUrlPath))
                {
                    return string.Empty;
                }
                else
                {
                    if (_NewUrlPath.EndsWith("/")) 
                        _NewUrlPath = _NewUrlPath.Substring(0, _NewUrlPath.Length - 1).Trim().ToLower().Replace(' ', '-').Replace("www.cancer.ca/en", "");
                        else _NewUrlPath = _NewUrlPath.Replace("www.cancer.ca/en", "");
                    return _NewUrlPath;
                }
            }
            set { _NewUrlPath = value; }
        }


        //[Description("To URL Hyperlink")]
        //public string ToUrlHL { get; set; }

        [Description("Notes")]
        public string Notes { get; set; }

    }
}
