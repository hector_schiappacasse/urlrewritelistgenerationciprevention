﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using static UrlRewriterListGenerator.Constants;
using Xls = UrlRewriterListGenerator.Constants.PageListCancerPreventionInfoXls;

namespace UrlRewriterListGenerator.Files.PreventionCancerInformation
{
   
    
        public class PageListCancerInfoFile
        {
            public ExcelPackage Package { get; }

            public PageListCancerInfoFile(ExcelPackage package)
            {
                Package = package;
            }

            public List<PageSource> GetAllSourcePages()
            {
                using (var ws = Package.GetWorksheet(Xls.WorksheetNames.PageList))
                {
                    var headerRow = ws.Dimension.Start.Row;
                    var topRow = headerRow + 5;
                    var endRow = ws.Dimension.End.Row;

                    var headerCells = ws.GetRowCells(5);
                    var itemId = headerCells.FindByText(Xls.ColumnNames.ItemId).GetCol();
                    var itemPath = headerCells.FindByText(Xls.ColumnNames.ItemPath).GetCol();
                    var englishUrl = headerCells.FindByText(Xls.ColumnNames.EnglishUrl).GetCol();
                    var frenchUrl = headerCells.FindByText(Xls.ColumnNames.FrenchUrl).GetCol();

                    var pageList = new List<PageSource>();
                    for (int row = topRow; row <= endRow; ++row)
                    {
                        var page = new PageSource
                        {
                            ItemId = ws.Cells[row, itemId].Text,
                            Source = RedirectSources.PageListSource,
                            SourceRow = row,
                            ItemPath = ws.Cells[row, itemPath].Text,
                            EnglishUrl = ws.Cells[row, englishUrl].Text,
                            FrenchUrl = ws.Cells[row, frenchUrl].Text
                        };
                        pageList.Add(page);
                    }
                    return pageList;
                }
            }
       }

 }

