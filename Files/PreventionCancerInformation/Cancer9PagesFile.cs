﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace UrlRewriterListGenerator.Files.PreventionCancerInformation
{
    public class Cancer9PagesFile
    {
        public string Source { get; set; }

        [Description("Source Row")]
        public int SourceRow { get; set; }

        [Description("Item ID")]
        public string ContentItemID { get; set; }

        [Description("Item Path")]
        public string ContentPath { get; set; }

            
        [Description("SC93 URL Path")]
        public string Sc93URLPath
        {
            get
            {
                //var toUri = AppUtils.GetAbsolutePath(this.ContentPath);
                var toUri = AppUtils.GetRelativePath(this.ContentPath);
                if (String.IsNullOrEmpty(toUri))
                {
                    SetStatus(RedirectStatus.ToUrlInvalid);
                    return string.Empty;
                }
                else
                {
                    SetStatus(RedirectStatus.OK);
                    return toUri;
                }

            }
        }

        [Description("Status Text")]
        public string StatusText { get; set; }

        [Description("Status")]
        public RedirectStatus Status { get; set; }


        public void SetStatus(RedirectStatus status, string statusText = null)
        {
            Status = status;
            StatusText = statusText;
        }


    }
}
