﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xls = UrlRewriterListGenerator.Constants.CPreventionRedirectsXLs;

namespace UrlRewriterListGenerator.Files.PreventionCancerInformation
{
    public class PreventionRedirectsFile
    {
        public PreventionRedirectsFile(ExcelPackage package)
        {
            Package = package;
        }

        public ExcelPackage Package { get; }

        public List<PreventionURLRedirects> GetAllUrls()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.CPreventionRedirects))
            {
                var headerRow = ws.Dimension.Start.Row;
                var topRow = headerRow + 3;
                var endRow = ws.Dimension.End.Row;

                var headerCells = ws.GetRowCells(3);

                var fromUrl = headerCells.FindByText(Xls.ColumnNames.FromUrl).GetCol();
                var toUrl = headerCells.FindByText(Xls.ColumnNames.ToUrl).GetCol();
                var oldPageTitleEn = headerCells.FindByText(Xls.ColumnNames.OldPageTitleEn).GetCol();
                var oldPageTitleFr = headerCells.FindByText(Xls.ColumnNames.OldPageTitleFr).GetCol();
                var newPageTitleEn = headerCells.FindByText(Xls.ColumnNames.newPageTitleEn).GetCol();
                var notes = headerCells.FindByText(Xls.ColumnNames.Notes).GetCol();


                var pageList = new List<PreventionURLRedirects>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    string fromUrlText = ws.Cells[row, fromUrl].Text;
                    string toUrlText = ws.Cells[row, toUrl].Text;
                    var page = new PreventionURLRedirects
                    {
                        //SourcePRow = row,
                        OldPageTitleEn = ws.Cells[row, oldPageTitleEn].Text,
                        OldPageTitleFr = ws.Cells[row, oldPageTitleFr].Text,
                        FromUrl = ws.Cells[row, fromUrl].Text,
                        NewPageTitleEn = ws.Cells[row, newPageTitleEn].Text,
                        ToUrl = ws.Cells[row, toUrl].Text,
                        Notes = ws.Cells[row, notes].Text,
                        OldUrlPath = ws.Cells[row, fromUrl].Text,
                        NewUrlPath = ws.Cells[row, toUrl].Text,

                        //FromUrl = fromUrlText.Contains("/en/") ? fromUrlText:"N/A" ,
                        //ToUrl = toUrlText.Contains("/en/") ? toUrlText : "N/A",
                    };
                    pageList.Add(page);
                }
                return pageList;
            }
        }
        public List<PreventionURLRedirects> GetAllEarlyCureUrls()
        {
            using (var ws = Package.GetWorksheet(Xls.WorksheetNames.CPreventionRedirectsProcessed))
            {
                var headerRow = ws.Dimension.Start.Row;
                int topRow;
                int headerCellsCount;
                if (ws.Name == "Find cancer early")
                {
                    topRow = headerRow + 1;
                    headerCellsCount = 1;
                }
                else
                {
                    topRow = headerRow + 3;
                    headerCellsCount = 3;
                }
            
                var endRow = ws.Dimension.End.Row;
                var headerCells = ws.GetRowCells(headerCellsCount);


                var fromUrl = headerCells.FindByText(Xls.ColumnNames.FromUrl).GetCol();
                var toUrl = headerCells.FindByText(Xls.ColumnNames.ToUrl).GetCol();
                var oldPageTitleEn = headerCells.FindByText(Xls.ColumnNames.OldPageTitleEn).GetCol();
                var oldPageTitleFr = headerCells.FindByText(Xls.ColumnNames.OldPageTitleFr).GetCol();
                var newPageTitleEn = headerCells.FindByText(Xls.ColumnNames.newPageTitleEn).GetCol();
                var notes = headerCells.FindByText(Xls.ColumnNames.Notes).GetCol();


                var pageList = new List<PreventionURLRedirects>();
                for (int row = topRow; row <= endRow; ++row)
                {
                    string fromUrlText = ws.Cells[row, fromUrl].Text;
                    string toUrlText = ws.Cells[row, toUrl].Text;
                    var page = new PreventionURLRedirects
                    {
                        //SourcePRow = row,
                        OldPageTitleEn = ws.Cells[row, oldPageTitleEn].Text,
                        OldPageTitleFr = ws.Cells[row, oldPageTitleFr].Text,
                        FromUrl = ws.Cells[row, fromUrl].Text,
                        NewPageTitleEn = ws.Cells[row, newPageTitleEn].Text,
                        ToUrl = ws.Cells[row, toUrl].Text,
                        Notes = ws.Cells[row, notes].Text,
                        OldUrlPath = ws.Cells[row, fromUrl].Text,
                        NewUrlPath = ws.Cells[row, toUrl].Text,
                        //FromUrl = fromUrlText.Contains("/en/") ? fromUrlText:"N/A" ,
                        //ToUrl = toUrlText.Contains("/en/") ? toUrlText : "N/A",
                    };
                    pageList.Add(page);
                }
                return pageList;
            }
        }
    }

    public enum PRredirectStatus
    {
        CI_NotIn_SC9 = 200,
        CI_NotIn_SC7 = 300,
        NotApplicable,
        Found = 0,
        SC72_NotIn_CI = 10,
        Drop = 35
    }
}
