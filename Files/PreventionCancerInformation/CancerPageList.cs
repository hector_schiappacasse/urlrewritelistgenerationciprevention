﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using Xls = UrlRewriterListGenerator.Constants.Cancer9PagesInfoXls;


namespace UrlRewriterListGenerator.Files.PreventionCancerInformation
{
        public class CancerPageList
        {
            public CancerPageList(ExcelPackage package)
            {
                Package = package;
            }

            public ExcelPackage Package { get; }

            public List<Cancer9PagesFile> GetAllUrls()
            {
                using (var ws = Package.GetWorksheet(Xls.WorksheetNames.CanPrevPages))
                {
                    var headerRow = ws.Dimension.Start.Row;
                    var topRow = headerRow + 1;
                    var endRow = ws.Dimension.End.Row;

                    var headerCells = ws.GetRowCells(headerRow);

                    var itemId = headerCells.FindByText(Xls.ColumnNames.ContentItemID).GetCol();
                    var itemPath = headerCells.FindByText(Xls.ColumnNames.ContentSC93).GetCol();
                                  

                    var pageList = new List<Cancer9PagesFile>();
                    for (int row = topRow; row <= endRow; ++row)
                    {
                        var page = new Cancer9PagesFile
                        {
                            SourceRow = row,
                            ContentItemID = ws.Cells[row, itemId].Text,
                            ContentPath = ws.Cells[row, itemPath].Text,
                           

                        };
                        pageList.Add(page);
                    }
                    return pageList;
                }
            }
        }

 }


