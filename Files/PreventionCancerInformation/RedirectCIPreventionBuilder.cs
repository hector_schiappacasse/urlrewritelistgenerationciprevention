﻿using System;
using System.Collections.Generic;
using System.Linq;
using UrlRewriterListGenerator.Files;
using UrlRewriterListGenerator.Files.CancerInfomation;
using UrlRewriterListGenerator.Files.PreventionCancerInformation;
using static UrlRewriterListGenerator.Constants;

namespace UrlRewriterListGenerator.Processors.PreventionCancerInformation
{ 
    public class RedirectCIPreventionBuilder

{
    public IEnumerable<Cancer9PagesFile> cancer9Pages { get; set; }
    public IEnumerable<PageSource> sc72Pages { get; set; }
    public IEnumerable<PreventionURLRedirects> ciPrevRedirects { get; set; }

    public RedirectCIPreventionBuilder(
        IEnumerable<Cancer9PagesFile> _cancer9Pages,
        IEnumerable<PageSource> _sc72Pages,
        IEnumerable<PreventionURLRedirects> _ciPreventionRedirects
    )
    {
        cancer9Pages = _cancer9Pages;
        sc72Pages = _sc72Pages;
        ciPrevRedirects = _ciPreventionRedirects;

    }


    internal object GetCiRedirectListUpdated()
    {
        throw new NotImplementedException();
    }

    internal IEnumerable<PreventionURLRedirects> ValidateCIVsCSFiles()
    {
        var camsSc93URLPath = cancer9Pages.Where(y => y.Sc93URLPath != null).Select(y => y.Sc93URLPath.Trim()).ToList();
        bool item;
        var CI_NotIn_Cams_To = ciPrevRedirects
            .Where(x =>
            {
                bool Is_ci_NotIn_Cams_To = camsSc93URLPath.All(y => y != AppUtils.GetRelativePath(x.ToUrl.Trim()));
               
               
                if (Is_ci_NotIn_Cams_To)
                {   
                    if (x.ToUrl == string.Empty)
                        x.StatusSC9 = PRredirectStatus.Drop;
                    else
                        x.StatusSC9 = PRredirectStatus.CI_NotIn_SC9;
                }
                else
                {
                    
                    x.StatusSC9 = PRredirectStatus.Found;
                }
                return Is_ci_NotIn_Cams_To;
            })
            .ToList();
            



            var sc72URLs = sc72Pages.Select(y => y.EnglishUrl).ToList();
            var CI_NotIn_SC72_From = ciPrevRedirects
            .Where(x =>
            {
                bool Is_ci_NotIn_SC72_To = sc72URLs.All(y => y != AppUtils.GetRelativePathFromAbsoluteUrl(x.FromUrl));
                if (Is_ci_NotIn_SC72_To)
                {
                    if (x.FromUrl == string.Empty)
                        x.StatusSC7 = PRredirectStatus.Drop;
                    else
                        x.StatusSC7 = PRredirectStatus.CI_NotIn_SC7;

                }
                else
                {
                    x.StatusSC7 = PRredirectStatus.Found;
                }
                return Is_ci_NotIn_SC72_To;
            })
            .ToList();

       
        return ciPrevRedirects;

        #region comments

        #endregion comments


    }

}


}
